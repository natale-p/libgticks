/** \file small_code.c
 * 
 * \brief Test if the architecture is supported.
 *
 * If the architecture is unsupported, the test will fail an
 * assertion.
 */
#include <libgticks.h>
#include "code.h"
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <sched.h>
/** \brief Test small code
 *
 * Good Output: Some ticks count\n
 * Bad Output: Assert failed
 */
int main(int argc, char **argv)
{
	initialize_code();
	struct sched_param schedparam;
	schedparam.sched_priority = 11;
	sched_setscheduler(0, SCHED_FIFO, &schedparam);

	uint64_t start, end;

	start = gticks();
	code_tobe_measured();
	end = gticks();
	
	assert(start != 0);
	assert(end != 0);
	assert (start != end);

	if (argc >= 2){
		printf("Ticks\tNanoseconds\n");
	}

	printf("%"PRIu64"\t%"PRIu64"\n", end-start, cyc2ns(end-start));

	exit(EXIT_SUCCESS);
}
