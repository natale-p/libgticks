/** \file compare_regfreq.c
 *
 * \brief Compare register update frequency values, obtained dynamically with
 * clock_gettime or gettimeofday
 *
 * These values are comparated with the static value, obtained from
 * /proc/cpuinfo.
 */
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <inttypes.h>
#include "libgticks.h"

/** \brief Do the test
 *
 */
int main() {
	struct timespec ts, te;
	uint32_t cpu_khz;
	uint64_t cycles[2], ns;

	clock_gettime(CLOCK_MONOTONIC, &ts);
	cycles[0] = gticks_serial();

	clock_gettime(CLOCK_MONOTONIC, &ts);
	cycles[0] = gticks_serial();

	usleep(1000000);

	cycles[1] = gticks_serial();
	clock_gettime(CLOCK_MONOTONIC, &te);

	ns = ((te.tv_sec-ts.tv_sec)*1000000000) +
		(te.tv_nsec-ts.tv_nsec);

	cpu_khz = (double)((cycles[1] - cycles[0])*1000000) / (ns);

	printf("Register frequency update calculated with clock_gettime: %u\n", cpu_khz);
	cpu_khz = __static_get_clockfreq();
	printf("Reading from /proc/cpuinfo has returned: %u\n", cpu_khz);

	/* TESTING gettimeofday */
	
	struct timezone tz;
	struct timeval tvstart, tvstop;
	uint32_t microsec, cpu_mhz;

	memset(&tz, 0, sizeof(tz));
	/* Get it in cache */

	gettimeofday(&tvstart, &tz);
	gettimeofday(&tvstart, &tz);
	cycles[0] = gticks_serial();

	usleep(1000000);

	cycles[1] = gticks_serial();
	gettimeofday(&tvstop, &tz);

	microsec = ((tvstop.tv_sec-tvstart.tv_sec)*1000000) +
		(tvstop.tv_usec-tvstart.tv_usec);

	cpu_mhz = (cycles[1]-cycles[0])/microsec;
	printf("Register frequency update calculated with gettimeofday %u\n", cpu_mhz*1000);

	exit(EXIT_SUCCESS);
}
