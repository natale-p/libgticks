/** \file overhead.c
 *
 * \brief Measure overhead of clock_gettime
 */
#include <stdlib.h>
#include <assert.h>
#include <sched.h>
#include "libgticks.h"
#include "code.h"

/** \brief Testing overhead of clock_gettime on your system
 *
 * Good output: Some overhead (lower is better)\n
 * Bad output: Assert failed
 *
 */
int main(int argc, char **argv)
{
	uint64_t start, end;
	struct timespec tpstart;
	struct sched_param schedparam;
	schedparam.sched_priority = 11;
	sched_setscheduler(0, SCHED_FIFO, &schedparam);

	/* Cache warmup */
	clock_gettime(CLOCK_MONOTONIC, &tpstart);
	clock_gettime(CLOCK_MONOTONIC, &tpstart);
	clock_gettime(CLOCK_MONOTONIC, &tpstart);

	start = gticks_serial();
	start = gticks_serial();
	start = gticks_serial();
	clock_gettime(CLOCK_MONOTONIC, &tpstart);
	end = gticks_serial();

	assert(start != 0 && end != 0);
	assert(end > start);
	assert(start > 0);

	if (argc >= 2) {
		printf("Overhead(ns):\n");
	}
	printf("%"PRIu64"\n", cyc2ns(end-start));

	exit(EXIT_SUCCESS);
}
