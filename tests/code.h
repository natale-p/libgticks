/** \file code.h
 *
 * \brief Definition to sample code to measure
 *
 * The code is inlined where you want.
 * For example: \verbatim
void fun()
{
	initialize_code();
	...
	code_tobe_measured();
	...
}
\endverbatim
 */

/** \brief Size for arrays
 */
#define _MSIZE 300

/** \brief Initialize code to measure, with
 * variables declaration under protected namespace _
 */
#define initialize_code() \
        double volatile _a[_MSIZE][_MSIZE]; \
        double volatile _b[_MSIZE][_MSIZE]; \
        double volatile _c[_MSIZE][_MSIZE]; \
        int _i; \
        int _j

/** \brief Code to measure
 *
 * It does an initialization of two arrays, and
 * do the sum of values into a third array.
 */
#define code_tobe_measured() \
do { \
        for(_i = 0; _i < _MSIZE; _i++) \
                for (_j = 0; _j < _MSIZE; _j++) { \
                        _a[_i][_j] = (double)_i; \
                        _b[_i][_j] = (double)_j; \
                        _c[_i][_j] = _a[_i][_j] / _b[_i][_j]; \
                } \
} while(0)
