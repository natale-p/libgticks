/** \file compare_clock_gettime.c
 *
 * \brief Compare ns from gticks and from clock_gettime
 *
 * Report ns calculated from gticks and from clock_gettime
 */
#include <libgticks.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <sched.h>
#include <assert.h>
#include <sched.h>

/** \brief Get nanosecond from struct timespec
 * \param start Period start
 * \param stop Period end
 * \return Nanoseconds
 */
static inline uint64_t nanoseconds_from_timespec(struct timespec start,
		struct timespec stop)
{
	return ((stop.tv_sec-start.tv_sec)*1000000000) + 
		(stop.tv_nsec-start.tv_nsec);
}

static uint64_t do_meas_clock(int clock, uint64_t ns)
{
	struct timespec tpone, tpstart, tpstop;
	clockid_t clocks[] = { CLOCK_REALTIME, CLOCK_MONOTONIC };	
	tpone.tv_nsec = ns;
	tpone.tv_sec = 0;
	clock_gettime(clocks[clock], &tpstart);
	nanosleep(&tpone, NULL);
	clock_gettime(clocks[clock], &tpstop);

	return nanoseconds_from_timespec(tpstart, tpstop);
}

static uint64_t do_meas_gticks(uint64_t ns)
{
	uint64_t start, end;
	struct timespec tpone;
	tpone.tv_nsec = ns;
	tpone.tv_sec = 0;
	start = gticks_serial();
	nanosleep(&tpone, NULL);
	end = gticks_serial();
	return end-start;
}
/** \brief Compare cyc2ns and clock_gettime
 *
 * Clock are (in order):
 * -# CLOCK_REALTIME
 * -# CLOCK_MONOTONIC
 *
 * Good output: Similar values
 * Bad output: Assertion failed, or different (many many) values
 */
int main(int argc, char **argv)
{
	int i;
       	uint64_t volatile clock_nsec;
	uint64_t volatile gticks_diff;
	uint64_t gticks_nsec, ns_to_sleep;

	struct sched_param schedparam;
	schedparam.sched_priority = 11;
	sched_setscheduler(0, SCHED_FIFO, &schedparam);

	if (argc < 3) {
		printf("Usage:\n\t./compare_clock_gettime clock_nr ns_to_sleep [option]\n");
		printf("\tWhere [option] could be -v (verbose) or none\n");
		printf("\t and clock_nr 0 for CLOCK_MONOTONIC or 1 for CLOCK_REALTIME\n");
		exit(EXIT_FAILURE);
	}

	i = atoi(argv[1]);
	ns_to_sleep = atoll(argv[2]);
	
	clock_nsec = do_meas_clock(i, ns_to_sleep);
	clock_nsec = do_meas_clock(i, ns_to_sleep);
	clock_nsec = do_meas_clock(i, ns_to_sleep);
	gticks_diff = do_meas_gticks(ns_to_sleep);
	gticks_diff = do_meas_gticks(ns_to_sleep);
	gticks_diff = do_meas_gticks(ns_to_sleep);

	gticks_nsec = cyc2ns(gticks_diff);

	if (argc > 3) {
		printf("ns with clock_gettime\tns with gticks\n");
	}
	printf("%"PRIu64"\t\t\t%"PRIu64"\n", clock_nsec, gticks_nsec);
	exit(EXIT_SUCCESS);
}
