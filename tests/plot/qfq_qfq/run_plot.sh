#!/bin/bash

testname=qfq_qfq
title="Misurazioni tempi su scheduler QFQ"
arch="PowerBook G4 PowerPC"

echo "set title \"$arch \n $title \n Tempi per ciclo\"; set terminal latex; set output '${testname}_ciclo.tex'; set xlabel \"Nanosecondi\"; set yrange [0:1000]; set ylabel \"Iterazioni\"; plot '${testname}_cycle_orig_toplot.dat' using 1:2 notitle with points, '' using 1:2:(1.0) t 'gettime' smooth acsplines with lines 4, '${testname}_cycle_gticks_toplot.dat' u 1:2 notitle with points, '' using 1:2:(1.0) t 'gticks' smooth acsplines with lines 5; set terminal svg; set output '${testname}_ciclo.svg'; replot" | gnuplot
echo "set title \" $arch \n $title \n Tempi totali\"; set terminal latex; set output '${testname}_totale.tex'; set xlabel \"Nanosecondi\"; set ylabel \"Iterazioni\"; set yrange [0:600]; plot '${testname}_total_orig_toplot.dat' using 1:2 notitle with points, '' using 1:2:(1.0) t 'gettime' smooth csplines with lines 4, '${testname}_total_gticks_toplot.dat' u 1:2 notitle with points, '' using 1:2:(1.0) t 'gticks' smooth csplines with lines 5; set terminal svg; set output '${testname}_totale.svg'; replot" | gnuplot
