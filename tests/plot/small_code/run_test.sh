#!/bin/bash

N=${1:-10000}
testname=small_code
title="Verifica della correttezza di gticks"

for i in $(seq 1 $N); do
	RIS=$(../../small_code)
	echo -en "$i\t$RIS\n" >> $testname.dat
done

cat ${testname}.dat | maphimbu -x 2 -d 1000 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_toplot.dat
