#!/bin/bash

testname=small_code
title="Verifica della correttezza di gticks"
arch="PowerBook G4 PowerPC"

echo "set title \"$arch \n $title\";set terminal latex; set output '$testname.tex'; set xlabel 'Ticks'; set ylabel 'Iterazioni'; plot '${testname}_toplot.dat' u 1:2 notitle with points, '' u 1:2:(1.0) smooth acsplines with lines t 'esecuzione' 5; set terminal svg; set output '$testname.svg';replot " | gnuplot

