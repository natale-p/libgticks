#!/bin/bash

testname=overhead_posix
title="Overhead (in ns) per le funzioni POSIX\ngettimeofday e clockgettime"
arch="PowerBook G4 PowerPC"

echo "set title \"$arch \n $title\";set terminal latex; set yrange [0:7500]; set output '$testname.tex'; set xlabel \"Nanosecondi\"; set ylabel \"Iterazioni\"; plot '${testname}_gettime_toplot.dat' u 1:2 notitle with points, '' u 1:2:(1.0) smooth acsplines with lines t 'gettimeofday' 4, '${testname}_clock_toplot.dat' u 1:2 notitle with points, '' u 1:2:(1.0) smooth acsplines with lines t 'clockgettime' 5; set terminal svg; set output '$testname.svg'; replot" | gnuplot
