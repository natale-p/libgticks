#!/bin/bash

N=${1:-10000}
testname=overhead_posix
title="Overhead (in ns) per le funzioni POSIX\ngettimeofday e clockgettime"

for i in $(seq 1 $N); do
	RIS=$(../../overhead_gettimeofday)
	echo -en "$i\t$RIS\n" >> ${testname}_gettime.dat
done

for i in $(seq 1 $N); do
	RIS=$(../../overhead_clock_gettime)
	echo -en "$i\t$RIS\n" >> ${testname}_clock.dat
done


cat ${testname}_gettime.dat | maphimbu -x 2 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_gettime_toplot.dat
cat ${testname}_clock.dat | maphimbu -x 2 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_clock_toplot.dat
