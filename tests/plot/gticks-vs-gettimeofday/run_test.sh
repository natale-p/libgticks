#!/bin/bash

N=${1:-10000}
testname=compare_gettimeofday
title="Comparazione tra gettimeofday e gticks\nper la misurazione di 1 microsecondo"

for i in $(seq 1 $N); do
	RIS=$(../../compare_gettimeofday)
	echo -en "$i\t$RIS" >> $testname.dat
	echo -en "\n" >> $testname.dat
done

cat $testname.dat | maphimbu -x 2 -d 100 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_gettime.dat
cat $testname.dat | maphimbu -x 3 -d 100 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_gticks.dat

