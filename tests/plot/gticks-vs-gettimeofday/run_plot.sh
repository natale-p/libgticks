#!/bin/bash

testname=compare_gettimeofday
title="Confronto tra gettimeofday e gticks"
arch="PowerBook G4 PowerPC"
lw=4

echo "set title \"$arch \n $title\";set terminal latex; set output '$testname.tex'; set xlabel \"Nanosecondi\"; set yrange [0:10000]; set ylabel \"Iterazioni\"; plot '${testname}_gticks.dat' using 1:2 notitle with points, '' using 1:2:(1.0) t 'gticks' smooth acsplines with lines lw $lw lt 1, '${testname}_gettime.dat' u 1:2 notitle with points, '' using 1:2:(1.0) t 'gettimeofday' smooth acsplines with lines lw $lw lt 3; set terminal svg; set output '$testname.svg'; replot" | gnuplot
