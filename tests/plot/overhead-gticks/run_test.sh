#!/bin/bash

N=${1:-10000}
testname=overhead
title="Overhead (in ns) per la chiamata a gticks"

for i in $(seq 1 $N); do
	RIS=$(../../overhead)
	echo -en "$i\t$RIS\n" >> ${testname}_serial.dat
done

for i in $(seq 1 $N); do
	RIS=$(../../overhead_serial)
	echo -en "$i\t$RIS\n" >> ${testname}_unserial.dat
done

cat ${testname}_serial.dat | maphimbu -x 2 -d 50 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_serial_toplot.dat
cat ${testname}_unserial.dat | maphimbu -x 2 -d 50 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_unserial_toplot.dat
