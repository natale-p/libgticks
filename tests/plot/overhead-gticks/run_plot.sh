#!/bin/bash

testname=overhead
title="Overhead (in ns) per la chiamata a gticks"
arch="PowerBook G4 PowerPC"

echo "set title \"$arch \n $title\";set yrange [0:6500]; set terminal latex; set output '$testname.tex'; set xlabel \"Nanosecondi\"; set ylabel \"Iterazioni\"; plot '${testname}_serial_toplot.dat' u 1:2 notitle with points, '' u 1:2:(1.0) smooth acsplines with lines t 'gticks (serial)' 5, '${testname}_unserial_toplot.dat' u 1:2 notitle with points, '' u 1:2:(1.0) smooth acsplines with lines t 'gticks' 4; set terminal svg; set output '$testname.svg'; replot" | gnuplot
