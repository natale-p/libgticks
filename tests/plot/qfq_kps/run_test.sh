#!/bin/bash

N=${1:-1000}
testname=qfq_kps

for i in $(seq 1 $N); do
	RIS=$(../../qfq-test_code/test -alg kps -d 1:500:3,2:800:100 -n 10m -qmax 10000 -qmin 20 | grep ": " | cut -d ':' -f2 | cut -d ' ' -f2)
	echo -en "$i" >> $testname.dat
	for j in $RIS; do
		echo -en "\t${j}" >> $testname.dat
	done
	echo -en "\n" >> $testname.dat
done

cat ${testname}.dat | maphimbu -x 2 -d 5 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_cycle_orig_toplot.dat
cat ${testname}.dat | maphimbu -x 3 -d 5 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_cycle_gticks_toplot.dat
cat ${testname}.dat | maphimbu -x 4 -d 0.01 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_total_orig_toplot.dat
cat ${testname}.dat | maphimbu -x 5 -d 0.01 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_total_gticks_toplot.dat
