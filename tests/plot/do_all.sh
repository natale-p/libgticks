#!/bin/bash

for i in gticks-vs-clock_gettime  gticks-vs-gettimeofday \
	overhead-gticks  overhead-posix small_code \
	qfq_rr qfq_qfq qfq_kps; 
do
	echo "Doing $i..."
	cd $i;
	rm *.dat
	./run_test.sh
	./run_plot.sh
	cd ../
	echo -en "Done\n"
done

