#!/bin/bash

N=${1:-10000}
ns=${2:-100}

arch="PowerBook G4 PowerPC"
title="Confronto tra clockgettime e gticks"
testname=compare_clock_gettime_${ns}

echo "set title \"$arch \n $title con MONOTONIC\"; set terminal latex; set output '${testname}_monotonic.tex'; set yrange [0:10000]; set xlabel \"Nanosecondi\"; set ylabel \"Iterazioni\"; plot '${testname}_monotonic_gettime_toplot.dat' using 1:2 notitle with points, '' using 1:2:(1.0) t 'clockgettime' smooth csplines with lines 4, '${testname}_monotonic_gticks_toplot.dat' using 1:2 notitle with points, '' using 1:2:(1.0) t 'gticks' smooth acsplines with lines 5; set terminal svg; set output '${testname}_monotonic.svg'; replot" | gnuplot
echo "set title \"$arch \n $title con REALTIME\"; set terminal latex; set output '${testname}_realtime.tex'; set yrange [0:10000]; set xlabel \"Nanosecondi\"; set ylabel \"Iterazioni\";  plot '${testname}_realtime_gettime_toplot.dat' using 1:2 notitle with points, '' using 1:2:(1.0) t 'clockgettime' smooth csplines with lines 4, '${testname}_realtime_gticks_toplot.dat' using 1:2 notitle with points, '' using 1:2:(1.0) t 'gticks' smooth acsplines with lines 5; set terminal svg; set output '${testname}_realtime.svg'; replot" | gnuplot
