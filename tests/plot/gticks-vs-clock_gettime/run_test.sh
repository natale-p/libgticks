#!/bin/bash

N=${1:-10000}
ns=${2:-100}

testname=compare_clock_gettime_${ns}

for i in $(seq 1 $N); do
	RIS=$(../../compare_clock_gettime 0 $ns)
	echo -en "$i\t$RIS" >> ${testname}_monotonic.dat
	echo -en "\n" >> ${testname}_monotonic.dat
done

for i in $(seq 1 $N); do
	RIS=$(../../compare_clock_gettime 1 $ns)
	echo -en "$i\t$RIS" >> ${testname}_realtime.dat
	echo -en "\n" >> ${testname}_realtime.dat
done

cat ${testname}_monotonic.dat | maphimbu -x 2 -d 100 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_monotonic_gettime_toplot.dat
cat ${testname}_monotonic.dat | maphimbu -x 3 -d 100 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_monotonic_gticks_toplot.dat
cat ${testname}_realtime.dat | maphimbu -x 2 -d 100 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_realtime_gettime_toplot.dat
cat ${testname}_realtime.dat | maphimbu -x 3 -d 100 | tr -s " " | sed -e 's/^ //g' | sort -n -k 1 > ${testname}_realtime_gticks_toplot.dat

