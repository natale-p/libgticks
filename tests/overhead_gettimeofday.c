/** \file overhead.c
 *
 * \brief Measure overhead of gettimeofday
 */
#include <stdlib.h>
#include <assert.h>
#include <sched.h>
#include "libgticks.h"
#include "code.h"

/** \brief Testing overhead of gettimeofday on your system
 *
 * Good output: Some overhead (lower is better)\n
 * Bad output: Assert failed
 *
 */
int main(int argc, char **argv)
{
	uint64_t start, end;
	struct timeval tvstart;
	struct sched_param schedparam;
	schedparam.sched_priority = 11;
	sched_setscheduler(0, SCHED_FIFO, &schedparam);

	/* Cache warmup */
	gettimeofday(&tvstart, NULL);
	gettimeofday(&tvstart, NULL);
	gettimeofday(&tvstart, NULL);

	start = gticks_serial();
	start = gticks_serial();
	start = gticks_serial();
	gettimeofday(&tvstart, NULL);
	end = gticks_serial();

	assert(start != 0 && end != 0);
	assert(end > start);
	assert(start > 0);

	if (argc >= 2) {
		printf("Overhead(ns):\n");
	}
	printf("%"PRIu64"\n", cyc2ns(end-start));

	exit(EXIT_SUCCESS);
}
