/** \file overhead.c
 *
 * \brief Measure overhead of gticks
 */
#include <stdlib.h>
#include <assert.h>
#include <sched.h>
#include <libgticks.h>
#include "code.h"

/** \brief Testing overhead of gticks on your system
 *
 * Good output: Some overhead (lower is better)\n
 * Bad output: Assert failed
 *
 */
int main(int argc, char **argv)
{
	uint64_t volatile start, end;
	struct sched_param schedparam;
	schedparam.sched_priority = 11;
	sched_setscheduler(0, SCHED_FIFO, &schedparam);

	start = gticks();
	end = gticks();

	assert(start != 0);
	assert(end != 0);

	if (argc >= 2) {
		printf("Overhead(ns)\n");
	}
	printf("%"PRIu64"\n",cyc2ns(end-start));

	exit(EXIT_SUCCESS);
}
