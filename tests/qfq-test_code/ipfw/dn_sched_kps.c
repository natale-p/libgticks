/*
 * Copyright (c) 2010 ....
 * All rights reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifdef _KERNEL
#include <sys/malloc.h>
#include <sys/socket.h>
#include <sys/socketvar.h>
#include <sys/kernel.h>
#include <sys/mbuf.h>
#include <sys/module.h>
#include <net/if.h>	/* IFNAMSIZ */
#include <netinet/in.h>
#include <netinet/ip_var.h>		/* ipfw_rule_ref */
#include <netinet/ip_fw.h>	/* flow_id */
#include <netinet/ip_dummynet.h>
#include <netinet/ipfw/dn_heap.h>
#include <netinet/ipfw/ip_dn_private.h>
#include <netinet/ipfw/dn_sched.h>
#else
#include <dn_test.h>
#endif

#define DN_SCHED_KPS	18 // XXX Where?

/* Maximum number of levels and number of buckets per level. */
#define KPS_MAX_LEVELS	32
#define KPS_MAX_BUCKETS	256

/* Mask used to retrieve the bucket number. */
#define KPS_BUCKETS_MASK	(KPS_MAX_BUCKETS - 1)

/* Allowed weights are in [1, 2^KPS_MAX_WSHIFT]. */
#define KPS_MAX_WSHIFT	16
#define KPS_MAX_WEIGHT	(1<<KPS_MAX_WSHIFT)

/* Maximum weight sum, AKA 1 (one). */
#define KPS_MAX_WSUM		(1UL << (KPS_MAX_WSHIFT + 1))

/* Bits used for fixed point arithmetics. */
#define KPS_FP_SHIFT		32

/* Bounds on packet size (compile time constants). */
#define KPS_MIN_PSHIFT	8
#define KPS_MTU_SHIFT		11

/* Shift to convert virtual times into slots. */
#define KPS_SLOT_SHIFT	(KPS_FP_SHIFT + KPS_MIN_PSHIFT -	\
				 KPS_MAX_WSHIFT - 1)

/* Shift to convert packet sizes into virtual times (for vtime calculation). */
#define KPS_ONE_SHIFT		(KPS_FP_SHIFT - KPS_MAX_WSHIFT - 1)

#ifndef INIT_LIST_HEAD
struct list_head {
	struct list_head *prev, *next;
};

#define INIT_LIST_HEAD(l) do {	(l)->prev = (l)->next = (l); } while (0)
#define list_empty(l)	( (l)->next == l )
static inline void
__list_add(struct list_head *new, struct list_head *prev,
	struct list_head *next)
{
	next->prev = new;
	new->next = next;
	new->prev = prev;
	prev->next = new;
}

static inline void
list_add_tail(struct list_head *new, struct list_head *head)
{
	__list_add(new, head->prev, head);
}

#define list_first_entry(pL, ty, member)	\
	(ty *)((char *)((pL)->next) - offsetof(ty, member))

static inline void
__list_del(struct list_head *prev, struct list_head *next)
{
	next->prev = prev;
	prev->next = next;
}

static inline void
list_del(struct list_head *entry)
{
	__list_del(entry->prev, entry->next);
	entry->next = entry->prev = NULL;
}
#endif /* INIT_LIST_HEAD */

typedef uint64_t	u64;
typedef int64_t		s64;
typedef uint32_t	u32;
typedef	uint32_t	bitmap;

#define test_bit(ix, pData)     ((*pData) & (1<<(ix)))
#define __set_bit(ix, pData)    (*pData) |= (1<<(ix))
#define __clear_bit(ix, pData)  (*pData) &= ~(1<<(ix))

/* SIWFQ flow descriptor. */
struct kps_class {
	struct dn_queue _q;
	/* Link to create a per-bucket linked list. */
	struct list_head list;	// tailq entry

	u64 S, F;	/* Precise timestamps of the class. */

	unsigned int weight;
	unsigned int lmax;	/* Max packet length for this class. */
	unsigned int level;	/* Class' level. */
};

/* Interleaved-Stratified Timer Wheel */
struct kps_istw {
	bitmap levelbits;	/* Bits set indicate flows at the given level. */

	/* Bits set indicate flows in the head bucket at the given level. */
	bitmap frontbits;

	int nr_flows[KPS_MAX_LEVELS];	/* Number of flows per each level. */

	/* The actual buckets. */
	struct list_head buckets[KPS_MAX_LEVELS][KPS_MAX_BUCKETS];
};

/* scheduler descriptor. */
struct kps_sched {
	u64 V;			/* Precise virtual time. */

	/* Active and blocked containers. */
	struct kps_istw blocked;
	struct kps_istw active;

	struct mq tx_queue;	/* List of packets ready to be transmitted. */

	unsigned long wsum;	/* Weights sum up to one, keep a counter to enforce it. */
};

static inline int kps_get_level(u32 mask)
{
	if (mask & 1)
		return 0;

	return ffs(mask >> 1);
}

static u64 kps_get_rel_rate(unsigned int weight)
{
        u64 rate = (u64)weight << KPS_FP_SHIFT;

        return rate / KPS_MAX_WSUM;
}

static inline int kps_floor_log2(unsigned int x)
{
        x |= x >> 1;
        x |= x >> 2;
        x |= x >> 4;
        x |= x >> 8;
        x |= x >> 16;

        return ffs((x ^ (x >> 1)) >> 1);
}

static int kps_get_rate_level(unsigned int plen, unsigned int rate)
{
        u64 slot_size = ((u64)plen << KPS_FP_SHIFT) / rate;

        return kps_floor_log2(slot_size >> KPS_MIN_PSHIFT);
}

#if 0
static int kps_change_class(struct Qdisc *sch, u32 classid, u32 parentid,
			      struct nlattr **tca, unsigned long *arg)
{
	struct kps_sched *q = qdisc_priv(sch);
	struct kps_class *cl = (struct kps_class *)*arg;
	struct nlattr *tb[TCA_KPS_MAX + 1];
	spinlock_t *lock;
	u32 weight, lmax;
	int err;

	if (tca[TCA_OPTIONS] == NULL)
		return -EINVAL;

	err = nla_parse_nested(tb, TCA_QFQ_MAX, tca[TCA_OPTIONS], kps_policy);
	if (err < 0)
		return err;

	if (tb[TCA_KPS_WEIGHT]) {
		weight = nla_get_u32(tb[TCA_KPS_WEIGHT]);
		if (!weight || weight > (1 << KPS_MAX_WSHIFT))
			return -EINVAL;
	} else
		weight = 1;

	if (tb[TCA_KPS_LMAX]) {
		lmax = nla_get_u32(tb[TCA_KPS_LMAX]);
		if (lmax < (1UL << KPS_MIN_PSHIFT) ||
		    lmax > (1UL << KPS_MTU_SHIFT))
			return -EINVAL;
	} else
		lmax = 1UL << KPS_MTU_SHIFT;

	if (cl != NULL) {
		if (tca[TCA_RATE]) {
			lock = qdisc_root_sleeping_lock(sch);
			err = gen_replace_estimator(&cl->bstats, &cl->rate_est,
						    lock, tca[TCA_RATE]);
			if (err)
				return err;
		}

		err = 0;
		sch_tree_lock(sch);
		if (tb[TCA_KPS_WEIGHT]) {
			if (q->wsum - cl->weight + weight > KPS_MAX_WSUM)
				return -EINVAL;
			q->wsum += weight - cl->weight;
			cl->weight = weight;
		}
		sch_tree_unlock(sch);

		return 0;
	}

	cl = kmem_cache_alloc(kps_pool, GFP_KERNEL | __GFP_ZERO);
	if (cl == NULL)
		return -ENOBUFS;

	cl->refcnt = 1;
	cl->common.classid = classid;
	cl->weight = weight;
	cl->rate = kps_get_rel_rate(weight);
	INIT_LIST_HEAD(&cl->list);
	cl->level = kps_get_rate_level(lmax, cl->rate);

	q->wsum += cl->weight;

	cl->qdisc = qdisc_create_dflt(qdisc_dev(sch), sch->dev_queue,
				      &pfifo_qdisc_ops, classid);
	if (cl->qdisc == NULL)
		cl->qdisc = &noop_qdisc;

	if (tca[TCA_RATE]) {
		lock = qdisc_root_sleeping_lock(sch);
		err = gen_replace_estimator(&cl->bstats, &cl->rate_est,
					    lock, tca[TCA_RATE]);
		if (err) {
			qdisc_destroy(cl->qdisc);
			kmem_cache_free(kps_pool, cl);
			return err;
		}
	}

	sch_tree_lock(sch);
	qdisc_class_hash_insert(&q->clhash, &cl->common);
	sch_tree_unlock(sch);

	qdisc_class_hash_grow(sch, &q->clhash);

	*arg = (unsigned long)cl;
	return 0;
}

static void kps_destroy_class(struct Qdisc *sch, struct kps_class *cl)
{
	//struct kps_sched *q = (struct kps_sched *)sch;

	/* Do not support the destruction of an active class... */

	gen_kill_estimator(&cl->bstats, &cl->rate_est);
	qdisc_destroy(cl->qdisc);
	kmem_cache_free(kps_pool, cl);
}

static int kps_delete_class(struct Qdisc *sch, unsigned long arg)
{
	struct kps_sched *q = qdisc_priv(sch);
	struct kps_class *cl = (struct kps_class *)arg;

	if (cl->filter_cnt > 0)
		return -EBUSY;

	sch_tree_lock(sch);

	kps_purge_queue(cl);
	qdisc_class_hash_remove(&q->clhash, &cl->common);

	if (--cl->refcnt == 0)
		kps_destroy_class(sch, cl);

	sch_tree_unlock(sch);
	return 0;
}
#endif

/* Compare two timestamps, handling wraparound. */
static inline int kps_gt(u64 a, u64 b)
{
	return (s64)(a - b) > 0;
}

static inline int kps_slot_gt(u64 a, u64 b)
{
	return (s64)((a << KPS_SLOT_SHIFT) - (b << KPS_SLOT_SHIFT)) > 0;
}

static inline void kps_calc_finish(struct kps_class *cl, unsigned int len)
{
	u64 delta = ((u64)len << KPS_FP_SHIFT)/cl->weight;
	cl->F = cl->S + delta;
}

static inline u64 kps_slot_to_bucket(u64 slot, int level)
{
	return (slot - (1 << level)) >> (level + 1);
}

static inline u64 kps_bucket_to_slot(u64 bucket, int level)
{
	return (bucket << (level + 1)) + (1 << level);
}

static inline u64 kps_round_slot(u64 slot, int level)
{
	return kps_bucket_to_slot(kps_slot_to_bucket(slot, level), level);
}

static inline u64 kps_vtime_slot(u64 t)
{
	return (u64)(t >> KPS_SLOT_SHIFT);
}

static inline struct list_head *kps_get_bucket(struct kps_istw *istw,
						 u64 slot, int level)
{
	int bucket = kps_slot_to_bucket(slot, level) & KPS_BUCKETS_MASK;

	return &istw->buckets[level][bucket];
}

static inline void kps_istw_insert(struct kps_istw *istw,
				     struct kps_class *cl,
				     u64 slot, int level)
{
	struct list_head *bucket = kps_get_bucket(istw, slot, level);

	istw->nr_flows[level]++;
	__set_bit(level, &istw->levelbits);
	list_add_tail(&cl->list, bucket);
}

/* Insert a flow into given ISTW container. */
static inline void kps_insert_flow(struct kps_sched *q,
				     struct kps_class *cl,
				     unsigned int len)
{
	u64 slot;

	kps_calc_finish(cl, len);

	/* Calculate the start time slot. */
	slot = kps_vtime_slot(cl->S) - (2ULL << cl->level);
	if (!kps_slot_gt(kps_round_slot(slot, cl->level),
			kps_vtime_slot(q->V))) {
		/* We're eligible, insert into active at the right slot. */
		slot = kps_vtime_slot(cl->F) + (2ULL << cl->level);
		kps_istw_insert(&q->active, cl, slot, cl->level);

		ND("ENQ %p ACT slot = %llx, len = %d, weight = %u",
		       cl, slot, len, cl->weight);
	} else {
		/* Ineligible, insert into blocked. */
		kps_istw_insert(&q->blocked, cl, slot, cl->level);
		ND("ENQ %p BLK slot = %llx, len = %d, weight = %u",
		       cl, slot, len, cl->weight);
	}
}

/* Remove the first flow from a slot/level. */
static inline struct kps_class *
kps_remove(struct kps_istw *istw, u64 slot, int level)
{
	struct list_head *bucket = kps_get_bucket(istw, slot, level);
	struct kps_class *cl;

	cl = list_first_entry(bucket, struct kps_class, list);
	list_del(&cl->list);

	istw->nr_flows[level]--;
	if (!istw->nr_flows[level])
		__clear_bit(level, &istw->levelbits);

	return cl;
}

static inline struct kps_class *
kps_istw_search(struct kps_istw *istw, u64 slot)
{
	int level, step;

	/* No flows, nothing to search. */
	if (!istw->levelbits)
		return NULL;

	/* Start from the first slot of the lowest level with some flows. */
	level = kps_get_level(istw->levelbits);
	slot = kps_round_slot(slot, level);

	/* Skip levels below the first non-empty one. */
	step = 1 << level;

	/*
	 * Iterate over the buckets, calculating on-the-fly the level of the
	 * slot we are looking up.
	 */
	while (list_empty(kps_get_bucket(istw, slot, level))) {
		slot += step;
		level = kps_get_level(slot);
	}

	/* Return the first element, removing it from the container.*/
	return kps_remove(istw, slot, level);
}

static struct kps_class *
kps_istw_scan(struct kps_istw *istw, u64 slot)
{
	int level = kps_get_level(slot);
	struct kps_class *cl;

	/* Update frontbits if we entered a non-empty slot. */
	if (!list_empty(kps_get_bucket(istw, slot, level)))
		__set_bit(level, &istw->frontbits);

	/* Nothing to return. */
	if (!istw->frontbits)
		return NULL;

	/* The first non-empty bucket is at the level indicated by frontbits. */
	level = kps_get_level(istw->frontbits);
	cl = kps_remove(istw, slot, level);

	/* Update frontbits if necessary. */
	if (list_empty(kps_get_bucket(istw, slot, level)))
		__clear_bit(level, &istw->frontbits);

	return cl;
}

struct mbuf *
kps_dequeue(struct dn_sch_inst *_si)
{
	struct kps_sched *q = (struct kps_sched *)(_si + 1);
	struct mbuf *m = NULL, *m2;
	struct kps_class *cl;
	u64 slot, sslot, old_vslot;
	struct list_head *bucket;
	int level;
	int len;

	old_vslot = kps_vtime_slot(q->V);
	m = q->tx_queue.head;
	if (m != NULL) {
		q->tx_queue.head = m->m_nextpkt;
		m->m_nextpkt = NULL;
		cl = NULL;
	} else {
		if (!q->active.levelbits)
			goto out;

		cl = kps_istw_search(&q->active, old_vslot);
		m = dn_dequeue(&cl->_q);
		if (!m)
			return NULL;

		/* Update flow timestamps. */
		cl->S = cl->F;
	}

	/* Update system virtual time. */
	q->V += (u64)m->m_pkthdr.len << KPS_ONE_SHIFT;

	if (cl && cl->_q.mq.head) {
		len = cl->_q.mq.head->m_pkthdr.len;
		kps_insert_flow(q, cl, len);
	}

	/*
	 * Unlike what's in the paper, it seems that we need to start one
	 * slot before the virtual time: While the slot is effectively cleared
	 * before the virtual time increases of the same span covered by
	 * the slot, it seems that due to unaligment between the beginning
	 * of the slot and the value of the virtual time, starting exactly
	 * from old_vslot leaves flows behind.  (Should prove that or
	 * something...)
	 */
	slot = old_vslot ; // - 1; // XXX do we need -1 ?
	while (kps_slot_gt(kps_vtime_slot(q->V), slot) &&
	       q->blocked.levelbits) {
		if (!q->active.levelbits && !q->blocked.frontbits) {
			cl = kps_istw_search(&q->blocked, slot);
			slot = kps_vtime_slot(cl->S);
			slot = kps_round_slot(slot, cl->level) -
				(2 << cl->level);

			/*
			 * Another difference with the paper: This should
			 * be the only case where virtual time jumps.
			 */
			if (kps_slot_gt(slot, kps_vtime_slot(q->V))) {
				q->V = slot << KPS_SLOT_SHIFT;
				old_vslot = slot;
				ND("JMP %llx/%llx\n", q->V, slot);
			}
		} else {
			cl = kps_istw_scan(&q->blocked, slot);
			slot++;
		}

		if (cl) {
			/* Do the transfer. */
			sslot = kps_vtime_slot(cl->F) +
				 (2ULL << cl->level);
			kps_istw_insert(&q->active, cl, sslot, cl->level);
		}
	}

	slot = old_vslot;
	while (kps_slot_gt(kps_vtime_slot(q->V), slot)) {
		level = kps_get_level(slot);
		bucket = kps_get_bucket(&q->active, slot, level);
		while (!list_empty(bucket)) {
			cl = kps_remove(&q->active, slot, level);
			m2 = dn_dequeue(&cl->_q);
			mq_append(&q->tx_queue, m2);
			cl->S = cl->F;
			if (cl->_q.mq.head) {
				len = cl->_q.mq.head->m_pkthdr.len;
				kps_insert_flow(q, cl, len);
			}
		}
		slot++;
	}

out:
	return m;
}

int
kps_enqueue(struct dn_sch_inst *_si, struct dn_queue *_q, struct mbuf *m)
{
	struct kps_sched *q = (struct kps_sched *)(_si + 1);
	struct kps_class *cl = (struct kps_class *)_q;
	u64 rlimit;

	/* verify length */
        if (m != _q->mq.head) {
                if (dn_enqueue(_q, m, 0)) /* packet was dropped */
                        return 1;
                if (m != _q->mq.head)
                        return 0;
        }

	if (m == _q->mq.head) {
		/*
		 * This ugly extra check comes from the need of avoiding
		 * flows with too old timestamps reappearing after a
		 * vtime wraparound and being put too far in the future.
		 */
		rlimit = q->V + (2ULL << cl->level);
		if (kps_gt(q->V, cl->S) ||
		    kps_gt(cl->S, rlimit))
			cl->S = q->V;
		else if (!q->active.levelbits)
			q->V = cl->S;

		kps_insert_flow(q, cl, m->m_pkthdr.len);
	}
	return 0;
}

static int
kps_new_queue(struct dn_queue *_q)
{
        struct kps_sched *q = (struct kps_sched *)(_q->_si + 1);
        struct kps_class *cl = (struct kps_class *)_q;
        uint32_t w;     /* approximated weight */
	u64 rate;

        w = _q->fs->fs.par[0];
        cl->lmax = _q->fs->fs.par[1];
        if (!w || w > KPS_MAX_WEIGHT) {
                w = 1;
                D("rounding weight to 1");
        }
	if (q->wsum + w > KPS_MAX_WSUM)
                return EINVAL;

	rate = kps_get_rel_rate(w);
	cl->weight = w;
	cl->level = kps_get_rate_level(cl->lmax, rate);
	return 0;
}

static int
kps_free_queue(struct dn_queue *_q)
{
	struct kps_sched *q = (struct kps_sched *)(_q->_si + 1);
	struct kps_class *cl = (struct kps_class *)(_q + 1);
	if (cl->weight) {
		q->wsum -= cl->weight;
		cl->weight = 0; /* reset weight to avoid run twice */
	}
	return 0;
}

static int
kps_new_fsk(struct dn_fsk *f)
{
	ipdn_bound_var(&f->fs.par[0], 1, 1, KPS_MAX_WEIGHT, "qfq weight");
	ipdn_bound_var(&f->fs.par[1], 1500, 1, 2000, "qfq maxlen");
	ND("weight %d len %d\n", f->fs.par[0], f->fs.par[1]);
	return 0;
}

static int
kps_new_sched(struct dn_sch_inst *si)
{
	struct kps_sched *q = (struct kps_sched *)(si + 1);

	int i, j;
	for (i = 0; i < KPS_MAX_LEVELS; i++) {
		for (j = 0; j < KPS_MAX_BUCKETS; j++) {
			INIT_LIST_HEAD(&q->blocked.buckets[i][j]);
			INIT_LIST_HEAD(&q->active.buckets[i][j]);
		}
	}
	return 0;
}

/*
 * SIWFQ scheduler descriptor
 */
static struct dn_alg kps_desc = {
	.type = DN_SCHED_KPS,
	.name = "KPS",
	.flags = DN_MULTIQUEUE,

	.si_datalen = sizeof(struct kps_sched),
	.q_datalen = sizeof(struct kps_class) - sizeof(struct dn_queue),

	.enqueue = kps_enqueue,
	.dequeue = kps_dequeue,
#if 0
	.config = rr_config,
#endif
	.new_sched = kps_new_sched,

	.new_fsk = kps_new_fsk,

	.new_queue = kps_new_queue,
	.free_queue = kps_free_queue,
};


DECLARE_DNSCHED_MODULE(dn_kps, &kps_desc);
