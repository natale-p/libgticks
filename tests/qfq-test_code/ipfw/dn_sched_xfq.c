/*
 * Copyright (c) 2010 Fabio Checconi, Luigi Rizzo, Paolo Valente
 * All rights reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifdef _KERNEL
#include <sys/malloc.h>
#include <sys/socket.h>
#include <sys/socketvar.h>
#include <sys/kernel.h>
#include <sys/mbuf.h>
#include <sys/module.h>
#include <net/if.h>	/* IFNAMSIZ */
#include <netinet/in.h>
#include <netinet/ip_var.h>		/* ipfw_rule_ref */
#include <netinet/ip_fw.h>	/* flow_id */
#include <netinet/ip_dummynet.h>
#include <netinet/ipfw/dn_heap.h>
#include <netinet/ipfw/ip_dn_private.h>
#include <netinet/ipfw/dn_sched.h>
#else
#include <dn_test.h>
#endif

#ifdef XFQ_DEBUG
struct xfq_sched;
static void dump_sched(struct xfq_sched *q, const char *msg);
#define	NO(x)	x
#else
#define NO(x)
#endif
#define DN_SCHED_XFQ	4 // XXX Where?
typedef uint64_t	u64;
typedef	unsigned int	bitmap;

#ifndef __FreeBSD__
#if defined (__i386__) || defined (__x86_64__)
static inline unsigned long __fls(unsigned long word)
{
	asm("bsr %1,%0"
	    : "=r" (word)
	    : "rm" (word));
	return word;
}
#endif
#if defined (__powerpc__) || defined (__POWERPC__)
static inline unsigned long __fls(unsigned long word)
{
	int bit;
	asm ("cntlzw %0,%1" : "=r" (bit) : "r" (word));
	return 31 - bit;
}
#endif

#else
static inline unsigned long __fls(unsigned long word)
{
	return fls(word) - 1;
}
#endif

#ifdef XFQ_DEBUG
int test_bit(int ix, bitmap *p)
{
	if (ix < 0 || ix > 31)
		D("bad index %d", ix);
	return *p & (1<<ix);
}
void __set_bit(int ix, bitmap *p)
{
	if (ix < 0 || ix > 31)
		D("bad index %d", ix);
	*p |= (1<<ix);
}
void __clear_bit(int ix, bitmap *p)
{
	if (ix < 0 || ix > 31)
		D("bad index %d", ix);
	*p &= ~(1<<ix);
}
#else /* !XFQ_DEBUG */
/* XXX do we have fast version, or leave it to the compiler ? */
#define test_bit(ix, pData)	((*pData) & (1<<(ix)))
#define __set_bit(ix, pData)	(*pData) |= (1<<(ix))
#define __clear_bit(ix, pData)	(*pData) &= ~(1<<(ix))
#endif /* !XFQ_DEBUG */
/*-------------------------------------------*/
/*

Virtual time computations.

S, F and V are all computed in fixed point arithmetic with
FRAC_BITS decimal bits.

   XFQ_MAX_INDEX is the maximum index allowed for a group. We need
  	one bit per index.
   XFQ_MAX_WSHIFT is the maximum power of two supported as a weight.
   The layout of the bits is as below:
  
                   [ MTU_SHIFT ][      FRAC_BITS    ]
                   [ MAX_INDEX    ][ MIN_SLOT_SHIFT ]
  				 ^.__grp->index = 0
  				 *.__grp->slot_shift
  
   where MIN_SLOT_SHIFT is derived by difference from the others.

The max group index corresponds to Lmax/w_min, where
Lmax=1<<MTU_SHIFT, w_min = 1 .
From this, and knowing how many groups (MAX_INDEX) we want,
we can derive the shift corresponding to each group.

Because we often need to compute
	F = S + len/w_i  and V = V + len/wsum
instead of storing w_i store the value
	inv_w = (1<<FRAC_BITS)/w_i
so we can do F = S + len * inv_w * wsum.
We use W_TOT in the formulas so we can easily move between
static and adaptive weight sum.

The per-scheduler-instance data contain all the data structures
for the scheduler: bitmaps and bucket lists.

 */
/*
 * Maximum number of consecutive slots occupied by backlogged classes
 * inside a group. This is approx lmax/lmin + 5.
 * XXX check because it poses constraints on MAX_INDEX
 */
#define XFQ_MAX_SLOTS	32
/*
 * Shifts used for class<->group mapping. Class weights are
 * in the range [1, XFQ_MAX_WEIGHT], we to map each class i to the
 * group with the smallest index that can support the L_i / r_i
 * configured for the class.
 *
 * grp->index is the index of the group; and grp->slot_shift
 * is the shift for the corresponding (scaled) sigma_i.
 *
 * When computing the group index, we do (len<<FP_SHIFT)/weight,
 * then compute an FLS (which is like a log2()), and if the result
 * is below the MAX_INDEX region we use 0 (which is the same as
 * using a larger len).
 */
#define XFQ_MAX_INDEX		19
#define XFQ_MAX_WSHIFT		16	/* log2(max_weight) */

#define	XFQ_MAX_WEIGHT		(1<<XFQ_MAX_WSHIFT)
#define XFQ_MAX_WSUM		(2*XFQ_MAX_WEIGHT)
//#define IWSUM	(q->i_wsum)
#define IWSUM	((1<<FRAC_BITS)/XFQ_MAX_WSUM)

#define FRAC_BITS		30	/* fixed point arithmetic */
#define ONE_FP			(1UL << FRAC_BITS)

#define XFQ_MTU_SHIFT		11	/* log2(max_len) */
#define XFQ_MIN_SLOT_SHIFT	(FRAC_BITS + XFQ_MTU_SHIFT - XFQ_MAX_INDEX)

/*
 * Possible group states, also indexes for the bitmaps array in
 * struct xfq_queue. We rely on ELIG and INELIG being numbered 0 and 1
 */
enum xfq_state { ELIG, INELIG, XFQ_MAX_STATE };

struct xfq_group;
/*
 * additional queue info. Some of this info should come from
 * the flowset, we copy them here for faster processing.
 * This is an overlay of the struct dn_queue
 */
struct xfq_class {
	struct dn_queue _q;
	uint64_t S, F;		/* flow timestamps (exact) */
	struct xfq_class *next; /* Link for the slot list. */

	/* group we belong to. In principle we would need the index,
	 * which is log_2(lmax/weight), but we never reference it
	 * directly, only the group.
	 */
	struct xfq_group *grp;

	/* these are copied from the flowset. */
	uint32_t	inv_w;	/* ONE_FP/weight */
	uint32_t 	lmax;	/* Max packet size for this flow. */
};

/* Group descriptor, see the paper for details.
 * Basically this contains the bucket lists
 */
struct xfq_group {
	uint64_t S, F;			/* group timestamps (approx). */
	unsigned int slot_shift;	/* Slot shift. */
	unsigned int index;		/* Group index. */
	unsigned int front;		/* Index of the front slot. */

	/*
	 * Bitmap with non-empty slots.  Using unsigned long as most
	 * bitops in Linux use it (i'd rather use u32 or u64 XXX)
	 */
	bitmap full_slots;

	/* Array of lists of active classes. */
	struct xfq_class *slots[XFQ_MAX_SLOTS];
};

/* scheduler instance descriptor. */
struct xfq_sched {
	uint64_t	V;		/* Precise virtual time. */
	uint32_t	wsum;		/* weight sum */
	NO(uint32_t	i_wsum;		/* ONE_FP/w_sum */
	uint32_t	_queued;	/* debugging */
	uint32_t	loops;	/* debugging */)
	bitmap bitmaps[XFQ_MAX_STATE];	/* Group bitmaps. */
	struct xfq_group groups[XFQ_MAX_INDEX + 1]; /* The groups. */
};

/*---- support functions ----------------------------*/

/* Generic comparison function, handling wraparound. */
static inline int xfq_gt(u64 a, u64 b)
{
	return (int64_t)(a - b) > 0;
}

/* Round a precise timestamp to its slotted value. */
static inline u64 xfq_round_down(u64 ts, unsigned int shift)
{
	return ts & ~((1ULL << shift) - 1);
}

/* return the pointer to the group with lowest index in the bitmap */
static inline struct xfq_group *xfq_ffs(struct xfq_sched *q,
					unsigned long bitmap)
{
	int index = ffs(bitmap) - 1; // zero-based
	return &q->groups[index];
}

/*
 * Calculate a flow index, given its weight and maximum packet length.
 * index = log_2(maxlen/weight) but we need to apply the scaling.
 * This is used only once at flow creation.
 */
static int xfq_calc_index(uint32_t inv_w, unsigned int maxlen)
{
	u64 slot_size = (u64)maxlen *inv_w;
	unsigned long size_map;
	int index = 0;

	size_map = (unsigned long)(slot_size >> XFQ_MIN_SLOT_SHIFT);
	if (!size_map)
		goto out;

	index = __fls(size_map) + 1;	// basically a log_2()
	index -= !(slot_size - (1ULL << (index + XFQ_MIN_SLOT_SHIFT - 1)));

	if (index < 0)
		index = 0;

out:
	ND("W = %d, L = %d, I = %d\n", ONE_FP/inv_w, maxlen, index);
	return index;
}
/*---- end support functions ----*/

/*-------- API calls --------------------------------*/
/*
 * Validate and copy parameters from flowset.
 */
static int
xfq_new_queue(struct dn_queue *_q)
{
	struct xfq_sched *q = (struct xfq_sched *)(_q->_si + 1);
	struct xfq_class *cl = (struct xfq_class *)_q;
	int i;
	uint32_t w;	/* approximated weight */

	/* import parameters from the flowset. They should be correct
	 * already.
	 */
	w = _q->fs->fs.par[0];
	cl->lmax = _q->fs->fs.par[1];
	if (!w || w > XFQ_MAX_WEIGHT) {
		w = 1;
		D("rounding weight to 1");
	}
	cl->inv_w = ONE_FP/w;
	w = ONE_FP/cl->inv_w;	
	if (q->wsum + w > XFQ_MAX_WSUM)
		return EINVAL;

	i = xfq_calc_index(cl->inv_w, cl->lmax);
	cl->grp = &q->groups[i];
	q->wsum += w;
	// XXX cl->S = q->V; ?
	// XXX compute q->i_wsum
	return 0;
}

/* remove an empty queue */
static int
xfq_free_queue(struct dn_queue *_q)
{
	struct xfq_sched *q = (struct xfq_sched *)(_q->_si + 1);
	struct xfq_class *cl = (struct xfq_class *)_q;
	if (cl->inv_w) {
		q->wsum -= ONE_FP/cl->inv_w;
		cl->inv_w = 0; /* reset weight to avoid run twice */
	}
	return 0;
}

/* Calculate a mask to mimic what would be ffs_from(). */
static inline unsigned long
mask_from(unsigned long bitmap, int from)
{
	return bitmap & ~((1UL << from) - 1);
}

/*
 * In principle
 *	q->bitmaps[dst] |= q->bitmaps[src] & mask;
 *	q->bitmaps[src] &= ~mask;
 * but we should make sure that src != dst
 */
static inline void
xfq_move_groups(struct xfq_sched *q, unsigned long mask, int src, int dst)
{
	q->bitmaps[dst] |= q->bitmaps[src] & mask;
	q->bitmaps[src] &= ~mask;
}


/*
 * perhaps
 *
	old_V ^= q->V;
	old_V >>= XFQ_MIN_SLOT_SHIFT;
	if (old_V) {
		...
	}
 *
 */
static inline void
xfq_make_eligible(struct xfq_sched *q, u64 old_V)
{
	unsigned long mask, vslot, old_vslot;

	vslot = q->V >> XFQ_MIN_SLOT_SHIFT;
	old_vslot = old_V >> XFQ_MIN_SLOT_SHIFT;

	if (vslot != old_vslot) {
		mask = (2UL << (__fls(vslot ^ old_vslot))) - 1;
		xfq_move_groups(q, mask, INELIG, ELIG);
	}
}

/*
 * XXX we should make sure that slot becomes less than 32.
 * This is guaranteed by the input values.
 * roundedS is always cl->S rounded on grp->slot_shift bits.
 */
static inline void
xfq_slot_insert(struct xfq_group *grp, struct xfq_class *cl, u64 roundedS)
{
	u64 slot = (roundedS - grp->S) >> grp->slot_shift;
	unsigned int i = (grp->front + slot) % XFQ_MAX_SLOTS;

	cl->next = grp->slots[i];
	grp->slots[i] = cl;
	__set_bit(slot, &grp->full_slots);
}

/*
 * remove the entry from the slot
 */
static inline void
xfq_front_slot_remove(struct xfq_group *grp)
{
	struct xfq_class **h = &grp->slots[grp->front];

	*h = (*h)->next;
	if (!*h)
		__clear_bit(0, &grp->full_slots);
}

/*
 * Returns the first full queue in a group. As a side effect,
 * adjust the bucket list so the first non-empty bucket is at
 * position 0 in full_slots.
 */
static inline struct xfq_class *
xfq_slot_scan(struct xfq_group *grp)
{
	int i;

	ND("grp %d full %x", grp->index, grp->full_slots);
	if (!grp->full_slots)
		return NULL;

	i = ffs(grp->full_slots) - 1; // zero-based
	if (i > 0) {
		grp->front = (grp->front + i) % XFQ_MAX_SLOTS;
		grp->full_slots >>= i;
	}

	return grp->slots[grp->front];
}

/*
 * adjust the bucket list. When the start time of a group decreases,
 * we move the index down (modulo XFQ_MAX_SLOTS) so we don't need to
 * move the objects. The mask of occupied slots must be shifted
 * because we use ffs() to find the first non-empty slot.
 * This covers decreases in the group's start time, but what about
 * increases of the start time ?
 * Here too we should make sure that i is less than 32
 */
static inline void
xfq_slot_rotate(struct xfq_sched *q, struct xfq_group *grp, u64 roundedS)
{
	unsigned int i = (grp->S - roundedS) >> grp->slot_shift;

	grp->full_slots <<= i;
	grp->front = (grp->front - i) % XFQ_MAX_SLOTS;
}


static inline void
xfq_update_eligible(struct xfq_sched *q, u64 old_V)
{
	bitmap ineligible = q->bitmaps[INELIG] ;

	if (ineligible) {
		if (!q->bitmaps[ELIG]) {
			struct xfq_group *grp;
			grp = xfq_ffs(q, ineligible);
			if (xfq_gt(grp->S, q->V))
				q->V = grp->S;
		}
		xfq_make_eligible(q, old_V);
	}
}

/*
 * Updates the class, returns true if also the group needs to be updated.
 */
static inline int
xfq_update_class(struct xfq_sched *q, struct xfq_group *grp,
	    struct xfq_class *cl)
{

	cl->S = cl->F;
	if (cl->_q.mq.head == NULL)  {
		xfq_front_slot_remove(grp);
	} else {
		unsigned int len;
		u64 roundedS;

		len = cl->_q.mq.head->m_pkthdr.len;
		cl->F = cl->S + (u64)len * cl->inv_w;
		roundedS = xfq_round_down(cl->S, grp->slot_shift);
		if (roundedS == grp->S)
			return 0;

		xfq_front_slot_remove(grp);
		xfq_slot_insert(grp, cl, roundedS);
	}
	return 1;
}

static struct mbuf *
xfq_dequeue(struct dn_sch_inst *si)
{
	struct xfq_sched *q = (struct xfq_sched *)(si + 1);
	struct xfq_group *grp;
	struct xfq_class *cl;
	struct mbuf *m;
	u64 old_V;

	NO(q->loops++;)
	if (!q->bitmaps[ELIG]) {
		NO(if (q->queued)
			dump_sched(q, "start dequeue");)
		return NULL;
	}

	grp = xfq_ffs(q, q->bitmaps[ELIG]);

	cl = grp->slots[grp->front];
	/* extract from the first bucket in the bucket list */
	m = dn_dequeue(&cl->_q);

	if (!m) {
		D("BUG/* non-workconserving leaf */");
		return NULL;
	}
	NO(q->queued--;)
	old_V = q->V;
	q->V += (u64)m->m_pkthdr.len * IWSUM;
	ND("m is %p F 0x%llx V now 0x%llx", m, cl->F, q->V);

	if (xfq_update_class(q, grp, cl)) {
		cl = xfq_slot_scan(grp);
		if (!cl) { /* group gone, remove from ELIG */
			__clear_bit(grp->index, &q->bitmaps[ELIG]);
			// grp->S = grp->F + 1; // XXX debugging only
		} else {
			u64 roundedS = xfq_round_down(cl->S, grp->slot_shift);

			if (grp->S == roundedS)
				goto skip_unblock;
			grp->S = roundedS;
			grp->F = roundedS + (2ULL << grp->slot_shift);
			/* remove from ELIG and put in the new set */
			__clear_bit(grp->index, &q->bitmaps[ELIG]);
			__set_bit(grp->index, 
				  &q->bitmaps[xfq_gt(grp->S, q->V)]);
		}
	}

skip_unblock:
	xfq_update_eligible(q, old_V);
	NO(if (!q->bitmaps[ELIG] && q->queued)
		dump_sched(q, "end dequeue");)

	return m;
}

/*
 * Assign a reasonable start time for a new flow k in group i.
 * Admissible values for \hat(F) are multiples of \sigma_i
 * no greater than V+\sigma_i . Larger values mean that
 * we had a wraparound so we consider the timestamp to be stale.
 *
 * If F is not stale and F >= V then we set S = F.
 * Otherwise we should assign S = V, but this may violate
 * the ordering in ELIG. So, if we have groups in ELIG, set S to
 * the F_j of the first group j which would be blocking us.
 * We are guaranteed not to move S backward because
 * otherwise our group i would still be blocked.
 */
static inline void
xfq_update_start(struct xfq_sched *q, struct xfq_class *cl)
{
	unsigned long mask;
	uint32_t limit, roundedF;
	int slot_shift = cl->grp->slot_shift;

	roundedF = xfq_round_down(cl->F, slot_shift);
	limit = xfq_round_down(q->V, slot_shift) + (1UL << slot_shift);

	if (!xfq_gt(cl->F, q->V) || xfq_gt(roundedF, limit)) {
		/* timestamp was stale */
		mask = mask_from(q->bitmaps[ELIG], cl->grp->index);
		if (mask) {
			struct xfq_group *next = xfq_ffs(q, mask);
			if (xfq_gt(roundedF, next->F)) {
				cl->S = next->F;
				return;
			}
		}
		cl->S = q->V;
	} else { /* timestamp is not stale */
		cl->S = cl->F;
	}
}

static int
xfq_enqueue(struct dn_sch_inst *si, struct dn_queue *_q, struct mbuf *m)
{
	struct xfq_sched *q = (struct xfq_sched *)(si + 1);
	struct xfq_group *grp;
	struct xfq_class *cl = (struct xfq_class *)_q;
	u64 roundedS;

	NO(q->loops++;)
	DX(4, "len %d flow %p inv_w 0x%x grp %d", m->m_pkthdr.len,
		_q, cl->inv_w, cl->grp->index);
	/* XXX verify that the packet obeys the parameters */
	if (m != _q->mq.head) {
		if (dn_enqueue(_q, m, 0)) /* packet was dropped */
			return 1;
		NO(q->queued++;)
		if (m != _q->mq.head)
			return 0;
	}
	/* If reach this point, queue q was idle */
	grp = cl->grp;
	xfq_update_start(q, cl); /* adjust start time */
	/* compute new finish time and rounded start. */
	cl->F = cl->S + (u64)(m->m_pkthdr.len) * cl->inv_w;
	roundedS = xfq_round_down(cl->S, grp->slot_shift);

	/*
	 * insert cl in the correct bucket.
	 * If cl->S >= grp->S we don't need to adjust the
	 * bucket list and simply go to the insertion phase.
	 * Otherwise grp->S is decreasing, we must make room
	 * in the bucket list, and also recompute the group state.
	 * Finally, if there were no flows in this group and nobody
	 * was in ELIG make sure to adjust V.
	 */
	if (grp->full_slots) {
		if (!xfq_gt(grp->S, cl->S))
			goto skip_update;
		/* create a slot for this cl->S */
		xfq_slot_rotate(q, grp, roundedS);
		/* group was surely ineligible, remove */
		__clear_bit(grp->index, &q->bitmaps[INELIG]);
	} else if (!q->bitmaps[ELIG] && xfq_gt(roundedS, q->V))
		q->V = roundedS;

	grp->S = roundedS;
	grp->F = roundedS + (2ULL << grp->slot_shift); // i.e. 2\sigma_i
	__set_bit(grp->index, &q->bitmaps[xfq_gt(grp->S, q->V)]);
	ND("new state %d 0x%x",
	   xfq_gt(grp->S, q->V), q->bitmaps[xfq_gt(grp->S, q->V)]);
	ND("S %llx F %llx V %llx", cl->S, cl->F, q->V);
skip_update:
	xfq_slot_insert(grp, cl, roundedS);

	return 0;
}



static int
xfq_new_fsk(struct dn_fsk *f)
{
	ipdn_bound_var(&f->fs.par[0], 1, 1, XFQ_MAX_WEIGHT, "xfq weight");
	ipdn_bound_var(&f->fs.par[1], 1500, 1, 2000, "xfq maxlen");
	ND("weight %d len %d\n", f->fs.par[0], f->fs.par[1]);
	return 0;
}

/*
 * initialize a new scheduler instance
 */
static int
xfq_new_sched(struct dn_sch_inst *si)
{
	struct xfq_sched *q = (struct xfq_sched *)(si + 1);
	struct xfq_group *grp;
	int i;

	for (i = 0; i <= XFQ_MAX_INDEX; i++) {
		grp = &q->groups[i];
		grp->index = i;
		grp->slot_shift = XFQ_MTU_SHIFT + FRAC_BITS -
					(XFQ_MAX_INDEX - i);
	}
	return 0;
}

/*
 * XFQ scheduler descriptor
 */
static struct dn_alg xfq_desc = {
	.type = DN_SCHED_XFQ,
	.name = "XFQ",
	.flags = DN_MULTIQUEUE,

	.si_datalen = sizeof(struct xfq_sched),
	.q_datalen = sizeof(struct xfq_class) - sizeof(struct dn_queue),

	.enqueue = xfq_enqueue,
	.dequeue = xfq_dequeue,

	.new_sched = xfq_new_sched,
	.new_fsk = xfq_new_fsk,

	.new_queue = xfq_new_queue,
	.free_queue = xfq_free_queue,
};

DECLARE_DNSCHED_MODULE(dn_xfq, &xfq_desc);

#ifdef XFQ_DEBUG
static void
dump_groups(struct xfq_sched *q, uint32_t mask)
{
	int i, j;

	for (i = 0; i < XFQ_MAX_INDEX + 1; i++) {
		struct xfq_group *g = &q->groups[i];

		if (0 == (mask & (1<<i)))
			continue;
		for (j = 0; j < XFQ_MAX_SLOTS; j++) {
			if (g->slots[j])
				D("    bucket %d %p", j, g->slots[j]);
		}
		D("full_slots 0x%x", g->full_slots);
		D("        %2d S 0x%20llx F 0x%llx %c", i,
			g->S, g->F,
			mask & (1<<i) ? '1' : '0');
	}
}

static void
dump_sched(struct xfq_sched *q, const char *msg)
{
	D("--- in %s: ---", msg);
	ND("loops %d queued %d V 0x%llx", q->loops, q->queued, q->V);
	D("    ELIG 0x%08x", q->bitmaps[ELIG]);
	D("    INELIG 0x%08x", q->bitmaps[INELIG]);
	dump_groups(q, 0xffffffff);
};
#endif /* XFQ_DEBUG */
