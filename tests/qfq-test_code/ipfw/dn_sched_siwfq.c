/*
 * net/sched/sch_siwfq.c         Stratified-interleaved something.
 *
 * Copyright (c) 2009 ...
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/bitops.h>
#include <linux/errno.h>
#include <linux/netdevice.h>
#include <linux/pkt_sched.h>
#include <net/sch_generic.h>
#include <net/pkt_sched.h>
#include <net/pkt_cls.h>

#ifdef USERSPACE
#define STATIC
//#define DEBUG
#else
#define STATIC static
#define TRACE
#define DEBUG
#endif

#ifdef DEBUG
#define my_BUG()	BUG()
#define my_BUG_ON(x)	BUG_ON(x)
#define Dprintf(x...)	printf(x)
#else
#define my_BUG()
#define my_BUG_ON(x)
#define Dprintf(x...)
#endif

#include "trace.h"


#ifdef __FreeBSD__
int       ffs(int) __pure2;	// FreeBSD
#endif
/* Maximum number of levels and number of buckets per level. */
#define SIWFQ_MAX_LEVELS	32
#define SIWFQ_MAX_BUCKETS	256

/* Mask used to retrieve the bucket number. */
#define SIWFQ_BUCKETS_MASK	(SIWFQ_MAX_BUCKETS - 1)

/* Allowed weights are in [1, 2^SIWFQ_MAX_WSHIFT]. */
#define SIWFQ_MAX_WSHIFT	16

/* Maximum weight sum, AKA 1 (one). */
#define SIWFQ_MAX_WSUM		(1UL << (SIWFQ_MAX_WSHIFT + 1))

/* Bits used for fixed point arithmetics. */
#define SIWFQ_FP_SHIFT		32

/* Bounds on packet size (compile time constants). */
#define SIWFQ_MIN_PSHIFT	8
#define SIWFQ_MTU_SHIFT		11

/* Shift to convert virtual times into slots. */
#define SIWFQ_SLOT_SHIFT	(SIWFQ_FP_SHIFT + SIWFQ_MIN_PSHIFT -	\
				 SIWFQ_MAX_WSHIFT - 1)

/* Shift to convert packet sizes into virtual times (for vtime calculation). */
#define SIWFQ_ONE_SHIFT		(SIWFQ_FP_SHIFT - SIWFQ_MAX_WSHIFT - 1)

/* Pool used to allocate SIWFQ classes. */
static struct kmem_cache *siwfq_pool;

/* SIWFQ flow descriptor. */
struct siwfq_class {
	/* Common Linux data structures. */
	struct Qdisc_class_common common;
	unsigned int refcnt;
	unsigned int filter_cnt;

	struct gnet_stats_queue qstats;
	struct gnet_stats_basic bstats;
	struct gnet_stats_rate_est rate_est;
	struct Qdisc *qdisc;

	/* Link to create a per-bucket linked list. */
	struct list_head list;

	/* Precise timestamps of the class. */
	u64 start;
	u64 finish;

	/* Class' weight and relative rate. */
	unsigned int weight, rate;

	/* Max packet length for this class. */
	unsigned int lmax;

	/* Class' level. */
	unsigned int level;
};

/* Interleaved-Stratified Timer Wheel */
struct siwfq_istw {
	/* Bits set indicate flows at the given level. */
	unsigned long levelbits;

	/* Bits set indicate flows in the head bucket at the given level. */
	unsigned long frontbits;

	/* Number of flows per each level. */
	int nr_flows[SIWFQ_MAX_LEVELS];

	/* The actual buckets. */
	struct list_head buckets[SIWFQ_MAX_LEVELS][SIWFQ_MAX_BUCKETS];
};

/* Queue descriptor. */
struct siwfq_sched {
	/* Common Linux stuff. */
	struct tcf_proto *filter_list;
	struct Qdisc_class_hash clhash;

	/* Precise virtual time. */
	u64 vtime;

	/* Active and blocked containers. */
	struct siwfq_istw blocked;
	struct siwfq_istw active;

	/* List of packets ready to be transmitted. */
	struct sk_buff_head tx_queue;

	/* Weights sum up to one, keep a counter to enforce it. */
	unsigned long wsum;
};

static struct siwfq_class *siwfq_find_class(struct Qdisc *sch, u32 classid)
{
	struct siwfq_sched *q = qdisc_priv(sch);
	struct Qdisc_class_common *clc;

	clc = qdisc_class_find(&q->clhash, classid);
	if (clc == NULL)
		return NULL;
	return container_of(clc, struct siwfq_class, common);
}

static void siwfq_purge_queue(struct siwfq_class *cl)
{
	unsigned int len = cl->qdisc->q.qlen;

	qdisc_reset(cl->qdisc);
	qdisc_tree_decrease_qlen(cl->qdisc, len);
}

static const struct nla_policy siwfq_policy[TCA_SIWFQ_MAX + 1] = {
	[TCA_SIWFQ_WEIGHT] = { .type = NLA_U32 },
	[TCA_SIWFQ_LMAX] = { .type = NLA_U32 },
};

static inline int siwfq_get_level(u32 mask)
{
	if (mask & 1)
		return 0;

	return ffs(mask >> 1);
}

static u64 siwfq_get_rel_rate(unsigned int weight)
{
        u64 rate = (u64)weight << SIWFQ_FP_SHIFT;

        return rate / SIWFQ_MAX_WSUM;
}

static inline int siwfq_floor_log2(unsigned int x)
{
        x |= x >> 1;
        x |= x >> 2;
        x |= x >> 4;
        x |= x >> 8;
        x |= x >> 16;

        return ffs((x ^ (x >> 1)) >> 1);
}

static int siwfq_get_rate_level(unsigned int plen, unsigned int rate)
{
        u64 slot_size = (u64)plen << SIWFQ_FP_SHIFT;

        do_div(slot_size, rate);

        return siwfq_floor_log2(slot_size >> SIWFQ_MIN_PSHIFT);
}

static int siwfq_change_class(struct Qdisc *sch, u32 classid, u32 parentid,
			      struct nlattr **tca, unsigned long *arg)
{
	struct siwfq_sched *q = qdisc_priv(sch);
	struct siwfq_class *cl = (struct siwfq_class *)*arg;
	struct nlattr *tb[TCA_SIWFQ_MAX + 1];
	spinlock_t *lock;
	u32 weight, lmax;
	int err;

	if (tca[TCA_OPTIONS] == NULL)
		return -EINVAL;

	err = nla_parse_nested(tb, TCA_QFQ_MAX, tca[TCA_OPTIONS], siwfq_policy);
	if (err < 0)
		return err;

	if (tb[TCA_SIWFQ_WEIGHT]) {
		weight = nla_get_u32(tb[TCA_SIWFQ_WEIGHT]);
		if (!weight || weight > (1 << SIWFQ_MAX_WSHIFT))
			return -EINVAL;
	} else
		weight = 1;

	if (tb[TCA_SIWFQ_LMAX]) {
		lmax = nla_get_u32(tb[TCA_SIWFQ_LMAX]);
		if (lmax < (1UL << SIWFQ_MIN_PSHIFT) ||
		    lmax > (1UL << SIWFQ_MTU_SHIFT))
			return -EINVAL;
	} else
		lmax = 1UL << SIWFQ_MTU_SHIFT;

	if (cl != NULL) {
		if (tca[TCA_RATE]) {
			lock = qdisc_root_sleeping_lock(sch);
			err = gen_replace_estimator(&cl->bstats, &cl->rate_est,
						    lock, tca[TCA_RATE]);
			if (err)
				return err;
		}

		err = 0;
		sch_tree_lock(sch);
		if (tb[TCA_SIWFQ_WEIGHT]) {
			if (q->wsum - cl->weight + weight > SIWFQ_MAX_WSUM)
				return -EINVAL;
			q->wsum += weight - cl->weight;
			cl->weight = weight;
		}
		sch_tree_unlock(sch);

		return 0;
	}

	cl = kmem_cache_alloc(siwfq_pool, GFP_KERNEL | __GFP_ZERO);
	if (cl == NULL)
		return -ENOBUFS;

	cl->refcnt = 1;
	cl->common.classid = classid;
	cl->weight = weight;
	cl->rate = siwfq_get_rel_rate(weight);
	INIT_LIST_HEAD(&cl->list);
	cl->level = siwfq_get_rate_level(lmax, cl->rate);

	q->wsum += cl->weight;

	cl->qdisc = qdisc_create_dflt(qdisc_dev(sch), sch->dev_queue,
				      &pfifo_qdisc_ops, classid);
	if (cl->qdisc == NULL)
		cl->qdisc = &noop_qdisc;

	if (tca[TCA_RATE]) {
		lock = qdisc_root_sleeping_lock(sch);
		err = gen_replace_estimator(&cl->bstats, &cl->rate_est,
					    lock, tca[TCA_RATE]);
		if (err) {
			qdisc_destroy(cl->qdisc);
			kmem_cache_free(siwfq_pool, cl);
			return err;
		}
	}

	sch_tree_lock(sch);
	qdisc_class_hash_insert(&q->clhash, &cl->common);
	sch_tree_unlock(sch);

	qdisc_class_hash_grow(sch, &q->clhash);

	*arg = (unsigned long)cl;
	return 0;
}

static void siwfq_destroy_class(struct Qdisc *sch, struct siwfq_class *cl)
{
	//struct siwfq_sched *q = (struct siwfq_sched *)sch;

	/* Do not support the destruction of an active class... */
	BUG_ON(!list_empty(&cl->list));

	gen_kill_estimator(&cl->bstats, &cl->rate_est);
	qdisc_destroy(cl->qdisc);
	kmem_cache_free(siwfq_pool, cl);
}

static int siwfq_delete_class(struct Qdisc *sch, unsigned long arg)
{
	struct siwfq_sched *q = qdisc_priv(sch);
	struct siwfq_class *cl = (struct siwfq_class *)arg;

	if (cl->filter_cnt > 0)
		return -EBUSY;

	sch_tree_lock(sch);

	siwfq_purge_queue(cl);
	qdisc_class_hash_remove(&q->clhash, &cl->common);

	if (--cl->refcnt == 0)
		siwfq_destroy_class(sch, cl);

	sch_tree_unlock(sch);
	return 0;
}

static unsigned long siwfq_get_class(struct Qdisc *sch, u32 classid)
{
	struct siwfq_class *cl = siwfq_find_class(sch, classid);

	if (cl != NULL)
		cl->refcnt++;

	return (unsigned long)cl;
}

static void siwfq_put_class(struct Qdisc *sch, unsigned long arg)
{
	struct siwfq_class *cl = (struct siwfq_class *)arg;

	if (--cl->refcnt == 0)
		siwfq_destroy_class(sch, cl);
}

static struct tcf_proto **siwfq_tcf_chain(struct Qdisc *sch, unsigned long cl)
{
	struct siwfq_sched *q = qdisc_priv(sch);

	if (cl)
		return NULL;

	return &q->filter_list;
}

static unsigned long siwfq_bind_tcf(struct Qdisc *sch, unsigned long parent,
				    u32 classid)
{
	struct siwfq_class *cl = siwfq_find_class(sch, classid);

	if (cl != NULL)
		cl->filter_cnt++;

	return (unsigned long)cl;
}

static void siwfq_unbind_tcf(struct Qdisc *sch, unsigned long arg)
{
	struct siwfq_class *cl = (struct siwfq_class *)arg;

	cl->filter_cnt--;
}

static int siwfq_graft_class(struct Qdisc *sch, unsigned long arg,
			     struct Qdisc *new, struct Qdisc **old)
{
	struct siwfq_class *cl = (struct siwfq_class *)arg;

	if (new == NULL) {
		new = qdisc_create_dflt(qdisc_dev(sch), sch->dev_queue,
					&pfifo_qdisc_ops, cl->common.classid);
		if (new == NULL)
			new = &noop_qdisc;
	}

	sch_tree_lock(sch);
	siwfq_purge_queue(cl);
	*old = cl->qdisc;
	cl->qdisc = new;
	sch_tree_unlock(sch);
	return 0;
}

static struct Qdisc *siwfq_class_leaf(struct Qdisc *sch, unsigned long arg)
{
	struct siwfq_class *cl = (struct siwfq_class *)arg;

	return cl->qdisc;
}

static int siwfq_dump_class(struct Qdisc *sch, unsigned long arg,
			    struct sk_buff *skb, struct tcmsg *tcm)
{
	struct siwfq_class *cl = (struct siwfq_class *)arg;
	struct nlattr *nest;

	tcm->tcm_parent	= TC_H_ROOT;
	tcm->tcm_handle	= cl->common.classid;
	tcm->tcm_info	= cl->qdisc->handle;

	nest = nla_nest_start(skb, TCA_OPTIONS);
	if (nest == NULL)
		goto nla_put_failure;
	NLA_PUT_U32(skb, TCA_SIWFQ_WEIGHT, cl->weight);
	NLA_PUT_U32(skb, TCA_SIWFQ_LMAX, cl->lmax);
	return nla_nest_end(skb, nest);

nla_put_failure:
	nla_nest_cancel(skb, nest);
	return -EMSGSIZE;
}

static int siwfq_dump_class_stats(struct Qdisc *sch, unsigned long arg,
				  struct gnet_dump *d)
{
	struct siwfq_class *cl = (struct siwfq_class *)arg;
	struct tc_siwfq_stats xstats;

	memset(&xstats, 0, sizeof(xstats));
	xstats.weight = cl->weight;
	xstats.lmax = cl->lmax;

	if (gnet_stats_copy_basic(d, &cl->bstats) < 0 ||
	    gnet_stats_copy_rate_est(d, &cl->rate_est) < 0 ||
	    gnet_stats_copy_queue(d, &cl->qdisc->qstats) < 0)
		return -1;

	return gnet_stats_copy_app(d, &xstats, sizeof(xstats));
}

static void siwfq_walk(struct Qdisc *sch, struct qdisc_walker *arg)
{
	struct siwfq_sched *q = qdisc_priv(sch);
	struct siwfq_class *cl;
	struct hlist_node *n;
	unsigned int i;

	if (arg->stop)
		return;

	for (i = 0; i < q->clhash.hashsize; i++) {
		hlist_for_each_entry(cl, n, &q->clhash.hash[i], common.hnode) {
			if (arg->count < arg->skip) {
				arg->count++;
				continue;
			}
			if (arg->fn(sch, (unsigned long)cl, arg) < 0) {
				arg->stop = 1;
				return;
			}
			arg->count++;
		}
	}
}

static struct siwfq_class *siwfq_classify(struct sk_buff *skb,
					  struct Qdisc *sch,
					  int *qerr)
{
	struct siwfq_sched *q = qdisc_priv(sch);
	struct siwfq_class *cl;
	struct tcf_result res;
	int result;

	if (TC_H_MAJ(skb->priority ^ sch->handle) == 0) {
		cl = siwfq_find_class(sch, skb->priority);
		if (cl != NULL)
			return cl;
	}

	*qerr = NET_XMIT_SUCCESS | __NET_XMIT_BYPASS;
	result = tc_classify(skb, q->filter_list, &res);
	if (result >= 0) {
#ifdef CONFIG_NET_CLS_ACT
		switch (result) {
		case TC_ACT_QUEUED:
		case TC_ACT_STOLEN:
			*qerr = NET_XMIT_SUCCESS | __NET_XMIT_STOLEN;
		case TC_ACT_SHOT:
			return NULL;
		}
#endif
		cl = (struct siwfq_class *)res.class;
		if (cl == NULL)
			cl = siwfq_find_class(sch, res.classid);
		return cl;
	}
	return NULL;
}

/* Compare two timestamps, handling wraparound. */
static inline int siwfq_gt64(u64 a, u64 b)
{
	return (s64)(a - b) > 0;
}

static inline void siwfq_calc_finish(struct siwfq_class *cl, unsigned int len)
{
	u64 delta = (u64)len << SIWFQ_FP_SHIFT;

	do_div(delta, cl->weight);
	cl->finish = cl->start + delta;
}

static inline u64 siwfq_slot_to_bucket(u64 slot, int level)
{
	return (slot - (1 << level)) >> (level + 1);
}

static inline u64 siwfq_bucket_to_slot(u64 bucket, int level)
{
	return (bucket << (level + 1)) + (1 << level);
}

static inline u64 siwfq_round_slot(u64 slot, int level)
{
	return siwfq_bucket_to_slot(siwfq_slot_to_bucket(slot, level), level);
}

static inline u64 siwfq_vtime_slot(u64 t)
{
	/*
	 * We need sign extension when left-shifting the slot number,
	 * otherwise we wouldn't be able to cope with wraparound in
	 * slot comparisons.
	 */
	return (u64)((s64)t >> SIWFQ_SLOT_SHIFT);
}

static inline struct list_head *siwfq_get_bucket(struct siwfq_istw *istw,
						 u64 slot, int level)
{
	int bucket = siwfq_slot_to_bucket(slot, level) & SIWFQ_BUCKETS_MASK;

	my_BUG_ON(level > 31);
	return &istw->buckets[level][bucket];
}

static inline void siwfq_istw_insert(struct siwfq_istw *istw,
				     struct siwfq_class *cl,
				     u64 slot, int level)
{
	struct list_head *bucket = siwfq_get_bucket(istw, slot, level);

	istw->nr_flows[level]++;
	__set_bit(level, &istw->levelbits);
	list_add_tail(&cl->list, bucket);
}

/* Insert a flow into given ISTW container. */
static inline void siwfq_insert_flow(struct siwfq_sched *q,
				     struct siwfq_class *cl,
				     unsigned int len)
{
	u64 slot;

	siwfq_calc_finish(cl, len);

	/* Calculate the start time slot. */
	slot = siwfq_vtime_slot(cl->start) - (2ULL << cl->level);
	if (!siwfq_gt64(siwfq_round_slot(slot, cl->level),
			siwfq_vtime_slot(q->vtime))) {
		/* We're eligible, insert into active at the right slot. */
		slot = siwfq_vtime_slot(cl->finish) + (2ULL << cl->level);
		siwfq_istw_insert(&q->active, cl, slot, cl->level);

		Dprintf("ENQ %x ACT slot = %llx, len = %d, weight = %lu\n",
		       cl->common.classid, slot, len, cl->weight);
	} else {
		/* Ineligible, insert into blocked. */
		siwfq_istw_insert(&q->blocked, cl, slot, cl->level);
		Dprintf("ENQ %x BLK slot = %llx, len = %d, weight = %lu\n",
		       cl->common.classid, slot, len, cl->weight);
	}
}

/* Remove the first flow from a slot/level. */
static inline struct siwfq_class *siwfq_remove(struct siwfq_istw *istw,
					u64 slot, int level)
{
	struct list_head *bucket = siwfq_get_bucket(istw, slot, level);
	struct siwfq_class *cl;

	my_BUG_ON(list_empty(bucket));

	cl = list_first_entry(bucket, struct siwfq_class, list);
	list_del(&cl->list);

	istw->nr_flows[level]--;
	if (!istw->nr_flows[level])
		__clear_bit(level, &istw->levelbits);

	return cl;
}

static inline struct siwfq_class *siwfq_istw_search(struct siwfq_istw *istw,
						    u64 slot)
{
	int level, step;

	/* No flows, nothing to search. */
	if (!istw->levelbits)
		return NULL;

	/* Start from the first slot of the lowest level with some flows. */
	level = siwfq_get_level(istw->levelbits);
	slot = siwfq_round_slot(slot, level);

	/* Skip levels below the first non-empty one. */
	step = 1 << level;

	/*
	 * Iterate over the buckets, calculating on-the-fly the level of the
	 * slot we are looking up.
	 */
	while (list_empty(siwfq_get_bucket(istw, slot, level))) {
		slot += step;
		level = siwfq_get_level(slot);
	}

	/* Return the first element, removing it from the container.*/
	return siwfq_remove(istw, slot, level);
}

static struct siwfq_class *siwfq_istw_scan(struct siwfq_istw *istw, u64 slot)
{
	int level = siwfq_get_level(slot);
	struct siwfq_class *cl;

	/* Update frontbits if we entered a non-empty slot. */
	if (!list_empty(siwfq_get_bucket(istw, slot, level)))
		__set_bit(level, &istw->frontbits);

	/* Nothing to return. */
	if (!istw->frontbits)
		return NULL;

	/* The first non-empty bucket is at the level indicated by frontbits. */
	level = siwfq_get_level(istw->frontbits);
	cl = siwfq_remove(istw, slot, level);

	/* Update frontbits if necessary. */
	if (list_empty(siwfq_get_bucket(istw, slot, level)))
		__clear_bit(level, &istw->frontbits);

	return cl;
}

#ifdef DEBUG
static void print_timestamps(struct siwfq_class *cl)
{
	Dprintf("%llx/%llx->%llx/%llx> ", cl->start,
		siwfq_vtime_slot(cl->start) - (2 << cl->level),
		cl->finish,
		siwfq_vtime_slot(cl->finish) + (2 << cl->level));
}

static void siwfq_istw_dump(struct siwfq_istw *istw, u64 slot)
{
	struct list_head *bucket;
	struct siwfq_class *cl;
	int i, level;

	Dprintf("levelbits = %lx, frontbits = %lx\n",
	       istw->levelbits, istw->frontbits);
	slot -= 1024;
	for (i = 0; i < 8192; i++) {
		level = siwfq_get_level(slot);
		bucket = siwfq_get_bucket(istw, slot, level);
		if (!list_empty(bucket)) {
			Dprintf("[%d, %llx] ", level, slot);
			list_for_each_entry(cl, bucket, list) {
				Dprintf("<%x ", cl->common.classid);
				print_timestamps(cl);
			}
			Dprintf("\n");
		}
		slot++;
	}
}

void siwfq_dump_queue(struct siwfq_sched *q)
{
	Dprintf("===== vtime = %llx (%llx) =====\nACTIVE: ",
		q->vtime, siwfq_vtime_slot(q->vtime));
	siwfq_istw_dump(&q->active, siwfq_vtime_slot(q->vtime));
	Dprintf("BLOCKED: ");
	siwfq_istw_dump(&q->blocked, siwfq_vtime_slot(q->vtime));
}
#else
inline void siwfq_dump_queue(struct siwfq_sched *q)
{
}
#endif

struct sk_buff *siwfq_dequeue(struct Qdisc *sch)
{
	struct siwfq_sched *q = qdisc_priv(sch);
	struct sk_buff *skb = NULL, *skb2;
	struct siwfq_class *cl;
	u64 slot, sslot, old_vslot;
	struct list_head *bucket;
	int level;

	int len;

	unsigned int val[3];
	unsigned long flags;

	local_irq_save(flags);
	trc_begin(val);

	old_vslot = siwfq_vtime_slot(q->vtime);
	if (!skb_queue_empty(&q->tx_queue)) {
		skb = __skb_dequeue(&q->tx_queue);
		cl = NULL;
	} else {
		if (!q->active.levelbits) {
			BUG_ON(q->blocked.levelbits);
			goto out;
		}

		cl = siwfq_istw_search(&q->active, old_vslot);
		skb = qdisc_dequeue_peeked(cl->qdisc);
		if (!skb) {
			BUG(); /* non-workconserving leaf */
			return NULL;
		}

		/* Update flow timestamps. */
		cl->start = cl->finish;
	}

	/* Update system virtual time. */
	q->vtime += (u64)qdisc_pkt_len(skb) << SIWFQ_ONE_SHIFT;

	if (cl && cl->qdisc->q.qlen) {
		len = qdisc_peek_len(cl->qdisc);
		siwfq_insert_flow(q, cl, len);
	}

	/*
	 * Unlike what's in the paper, it seems that we need to start one
	 * slot before the virtual time: While the slot is effectively cleared
	 * before the virtual time increases of the same span covered by
	 * the slot, it seems that due to unaligment between the beginning
	 * of the slot and the value of the virtual time, starting exactly
	 * from old_vslot leaves flows behind.  (Should prove that or
	 * something...)
	 */
	slot = old_vslot - 1;
	while (siwfq_gt64(siwfq_vtime_slot(q->vtime), slot) &&
	       q->blocked.levelbits) {
		if (!q->active.levelbits && !q->blocked.frontbits) {
			cl = siwfq_istw_search(&q->blocked, slot);
			slot = siwfq_vtime_slot(cl->start);
			slot = siwfq_round_slot(slot, cl->level) -
				(2 << cl->level);

			/*
			 * Another difference with the paper: This should
			 * be the only case where virtual time jumps.
			 */
			if (siwfq_gt64(slot, siwfq_vtime_slot(q->vtime))) {
				q->vtime = slot << SIWFQ_SLOT_SHIFT;
				old_vslot = slot;
				Dprintf("JMP %llx/%llx\n", q->vtime, sslot);
			}
		} else {
			cl = siwfq_istw_scan(&q->blocked, slot);
			slot++;
		}

		if (cl) {
			/* Do the transfer. */
			sslot = siwfq_vtime_slot(cl->finish) +
				 (2ULL << cl->level);
			siwfq_istw_insert(&q->active, cl, sslot, cl->level);
		}
	}

	slot = old_vslot;
	while (siwfq_gt64(siwfq_vtime_slot(q->vtime), slot)) {
		level = siwfq_get_level(slot);
		bucket = siwfq_get_bucket(&q->active, slot, level);
		while (!list_empty(bucket)) {
			cl = siwfq_remove(&q->active, slot, level);
			skb2 = qdisc_dequeue_peeked(cl->qdisc);
			__skb_queue_tail(&q->tx_queue, skb2);
			cl->start = cl->finish;
			if (cl->qdisc->q.qlen) {
				len = qdisc_peek_len(cl->qdisc);
				siwfq_insert_flow(q, cl, len);
			}
		}
		slot++;
	}

	sch->q.qlen--;

out:
	trc_end(0, val);
	local_irq_restore(flags);

	return skb;
}

int siwfq_enqueue(struct sk_buff *skb, struct Qdisc *sch)
{
	struct siwfq_sched *q = qdisc_priv(sch);
	struct siwfq_class *cl;
	unsigned int len;
	int err = 0;
	unsigned int val[3];
	unsigned long flags;
	u64 rlimit;

	local_irq_save(flags);
	trc_begin(val);
	cl = siwfq_classify(skb, sch, &err);
	if (cl == NULL || cl->qdisc->q.qlen > 80) {
		if (err & __NET_XMIT_BYPASS)
			sch->qstats.drops++;
		local_irq_restore(flags);
		kfree_skb(skb);
		return err;
	}
	trc_end(2, val);
	trc_begin(val);

	len = qdisc_pkt_len(skb);
	if (len > cl->lmax)
		goto drop;
	err = qdisc_enqueue(skb, cl->qdisc);
	if (unlikely(err != NET_XMIT_SUCCESS)) {
drop:
		if (net_xmit_drop_count(err)) {
			cl->qstats.drops++;
			sch->qstats.drops++;
		}
		local_irq_restore(flags);
		return err;
	}

	my_BUG_ON(cl->qdisc->q.qlen > 100);

	if (cl->qdisc->q.qlen == 1) {
		/*
		 * This ugly extra check comes from the need of avoiding
		 * flows with too old timestamps reappearing after a
		 * vtime wraparound and being put too far in the future.
		 */
		rlimit = q->vtime + (2ULL << (cl->level + SIWFQ_MIN_PSHIFT));
		if (siwfq_gt64(q->vtime, cl->start) ||
		    siwfq_gt64(cl->start, rlimit))
			cl->start = q->vtime;
		else if (!q->active.levelbits)
			q->vtime = cl->start;

		siwfq_insert_flow(q, cl, len);
	}

	trc_end(1, val);
	local_irq_restore(flags);

	cl->bstats.packets++;
	cl->bstats.bytes += len;
	sch->bstats.packets++;
	sch->bstats.bytes += len;

	sch->q.qlen++;

	return err;
}

static void siwfq_qlen_notify(struct Qdisc *sch, unsigned long arg)
{
	//struct siwfq_sched *q = (struct siwfq_sched *)sch;
	struct siwfq_class *cl = (struct siwfq_class *)arg;

	if (cl->qdisc->q.qlen == 0)
		/* XXX deactivate cl */;
}

static unsigned int siwfq_drop(struct Qdisc *sch)
{
	/* XXX no drop support. */
	return 0;
}

int siwfq_init_qdisc(struct Qdisc *sch, struct nlattr *opt)
{
#if 0
	/* XXX */
	skb_queue_head_init(&q->tx_queue);
#ifdef TRACE
	global_qdisc = sch;
#endif
#endif

	return 0;
}

static void siwfq_reset_qdisc(struct Qdisc *sch)
{
	struct siwfq_sched *q = qdisc_priv(sch);
	struct siwfq_class *cl;
	struct hlist_node *n;
	unsigned int i;

	for (i = 0; i < q->clhash.hashsize; i++) {
		hlist_for_each_entry(cl, n, &q->clhash.hash[i], common.hnode) {
			if (cl->qdisc->q.qlen)
				/* XXX deactivate */;
			qdisc_reset(cl->qdisc);
		}
	}
	sch->q.qlen = 0;
}

static void siwfq_destroy_qdisc(struct Qdisc *sch)
{
	struct siwfq_sched *q = qdisc_priv(sch);
	struct siwfq_class *cl;
	struct hlist_node *n, *next;
	unsigned int i;

	tcf_destroy_chain(&q->filter_list);

	for (i = 0; i < q->clhash.hashsize; i++) {
		hlist_for_each_entry_safe(cl, n, next, &q->clhash.hash[i],
					  common.hnode)
			siwfq_destroy_class(sch, cl);
	}
	qdisc_class_hash_destroy(&q->clhash);

#ifdef TRACE
	if (global_qdisc == sch)
		global_qdisc = NULL;
#endif
}

#ifndef USERSPACE
static
#endif
const struct Qdisc_class_ops siwfq_class_ops = {
	.change		= siwfq_change_class,
	.delete		= siwfq_delete_class,
	.get		= siwfq_get_class,
	.put		= siwfq_put_class,
	.tcf_chain	= siwfq_tcf_chain,
	.bind_tcf	= siwfq_bind_tcf,
	.unbind_tcf	= siwfq_unbind_tcf,
	.graft		= siwfq_graft_class,
	.leaf		= siwfq_class_leaf,
	.qlen_notify	= siwfq_qlen_notify,
	.dump		= siwfq_dump_class,
	.dump_stats	= siwfq_dump_class_stats,
	.walk		= siwfq_walk,
};

#ifndef USERSPACE
static
#endif
struct Qdisc_ops qfq_qdisc_ops __read_mostly = {
	.cl_ops		= &siwfq_class_ops,
	.id		= "siwfq",
	.priv_size	= sizeof(struct siwfq_sched),
	.enqueue	= siwfq_enqueue,
	.dequeue	= siwfq_dequeue,
	.peek		= qdisc_peek_dequeued,
	.drop		= siwfq_drop,
	.init		= siwfq_init_qdisc,
	.reset		= siwfq_reset_qdisc,
	.destroy	= siwfq_destroy_qdisc,
	.owner		= THIS_MODULE,
};

#ifdef TRACE
static ssize_t siwfq_read(struct file *file, char __user *buf,
			  size_t count, loff_t *ppos)
{
	unsigned int event, values[3];
	int i = 0;
	static int max;

	if (!global_qdisc)
		return 0;

	if (*ppos == 0xdeadbeef) {
		ev_first = ev_last;
		return 0;
	}

	rtnl_lock();
	if (!*ppos)
		max = ev_num > 65534 ? 65534 : ev_num;
	if (max <= 0)
		goto out;
	for (i = 0; i < count / 16; max--, i += 4) {
		sch_tree_lock(global_qdisc);
		if (trc_get(&event, values)) {
			sch_tree_unlock(global_qdisc);
			break;
		}
		sch_tree_unlock(global_qdisc);
		put_user(event, (unsigned int *)buf + i);
		put_user(values[0], (unsigned int *)buf + i + 1);
		put_user(values[1], (unsigned int *)buf + i + 2);
		put_user(values[2], (unsigned int *)buf + i + 3);
	}
out:
	rtnl_unlock();

	return i * 16;
}

static ssize_t siwfq_write(struct file *file, const char __user *buf,
			   size_t count, loff_t *ppos)
{
	return 0;
}

static int siwfq_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int siwfq_release(struct inode *inode, struct file *file)
{
	return 0;
}

static const struct file_operations siwfq_dev_fops = {
	.owner		= THIS_MODULE,
	.llseek		= no_llseek,
	.read		= siwfq_read,
	.write		= siwfq_write,
	.open		= siwfq_open,
	.release	= siwfq_release,
};

static struct miscdevice siwfq_miscdev = {
	.minor		= MISC_DYNAMIC_MINOR,
	.name		= "siwfq",
	.fops		= &siwfq_dev_fops,
};
#endif /* TRACE */

#ifndef USERSPACE
static int __init siwfq_init(void)
{
	siwfq_pool = KMEM_CACHE(siwfq_class, 0);
	if (!siwfq_pool)
		return -ENOMEM;
	trc_init();
	misc_register(siwfq_miscdev);
	return register_qdisc(&siwfq_qdisc_ops);
}

static void __exit siwfq_exit(void)
{
	trc_exit();
	kmem_cache_destroy(siwfq_pool);
	misc_deregister(&siwfq_miscdev);
	unregister_qdisc(&siwfq_qdisc_ops);
}

module_init(siwfq_init);
module_exit(siwfq_exit);
MODULE_LICENSE("GPL");
#endif
