/** \file compare_gettimeofday.c
 *
 * \brief Compare microseconds from gticks and from gettimeofday
 *
 * Report microseconds calculated from gticks and from gettimeofday
 */
#include <libgticks.h>
#include <stdlib.h>
#include <sched.h>
#include <assert.h>
/** \brief Compare (cyc2ns/1000) and gettimeofday
 *
 * Good output: Similar values
 * Bad output: Assertion failed, or very different values
 */
int main(int argc, char** argv)
{
	struct timeval tvstop, tvstart;
	struct timespec tpone;
	uint64_t volatile start,end;
	uint64_t microseconds_1, microseconds_2;
	int i;

	struct sched_param schedparam;
	schedparam.sched_priority = 11;
	sched_setscheduler(0, SCHED_FIFO, &schedparam);

	tpone.tv_sec = 0;
	tpone.tv_nsec = 1000;

	for (i=0;i<2;i++) {	
		gettimeofday(&tvstart, NULL);
		nanosleep(&tpone, NULL);
		gettimeofday(&tvstop, NULL);
	}

	for (i=0;i<2;i++) {
		start = gticks_serial();
		nanosleep(&tpone, NULL);
		end = gticks_serial();
	}

	assert(end != 0 && start != 0);
	microseconds_1 = ((tvstop.tv_sec-tvstart.tv_sec)*1000000) +
		(tvstop.tv_usec-tvstart.tv_usec);

	microseconds_2 = cyc2ns(end-start);
	if (argc >= 2) {
		printf("ns with gettimeofday\tns with gticks\n");
	}
	printf("%"PRIu64"\t\t\t%"PRIu64"\n",microseconds_1*1000,microseconds_2);

	exit(EXIT_SUCCESS);
}
