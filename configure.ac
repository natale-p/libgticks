AC_INIT(libgticks,0.1,65252@studenti.unimore.it)
AM_INIT_AUTOMAKE([-Wall -Werror])
AC_CANONICAL_HOST

dnl find and test the C compiler
CFLAGS=""
AC_PROG_CC([gcc])
if test "$GCC" != yes ; then
	AC_MSG_ERROR(no gcc available on your system)
fi
AC_LANG_C
AC_C_INLINE
AC_C_VOLATILE

AC_TYPE_UINT32_T
AC_TYPE_UINT64_T

AC_CHECK_HEADERS([inttypes.h] [stdio.h] [stdarg.h] [stdint.h] [sys/time.h] [fcntl.h])
AC_CHECK_FUNCS([gettimeofday])
AC_CHECK_LIB([rt],[clock_gettime], [LIBRT=true], AC_MSG_ERROR(clock_gettime not
	     found in -lrt))

AC_ARG_ENABLE([static-regfreq],[AS_HELP_STRING([--enable-static-regfreq],
	                    [Read from /proc/cpuinfo instead of calculating
			     register update frequency value])],
			    [],[enable_static_regfreq=check])
if test "$enable_static_regfreq" != yes ; then
	AC_DEFINE(DYNAMIC_REGFREQ, [1],
                    [Define to 1 if you want to use a dynamic routine to get
		     register update frequency value])
	AC_MSG_NOTICE([Dynamically get register update frequency])
else
	AC_MSG_NOTICE([Statically get register update frequency from
		       /proc/cpuinfo])
fi	

# Check for 'cat' and get full path.
AC_ARG_VAR([CAT],[the 'cat' program used for compiling libgticks])
AC_PATH_PROG([CAT],[cat],[])
if test "x$CAT" = "x"; then
	AC_MSG_NOTICE([==> cat command not found!])
	AC_MSG_NOTICE([==> Set CAT variable if present in non-standard path!])
	AC_MSG_ERROR([cat is mandatory: will stop here!])
fi

# Check for 'printf' and get full path.
AC_ARG_VAR([PRINTF],[the 'printf' program used to print test information])
AC_PATH_PROG([PRINTF],[printf],[])
if test "x$PRINTF" = "x"; then
	AC_MSG_NOTICE([==> printf command not found!])
	AC_MSG_NOTICE([==> Set PRINTF variable if present in non-standard path!])
	AC_MSG_ERROR([printf is mandatory to run the tests : will stop here!])
fi

# Check for 'mv' and get full path.
AC_ARG_VAR([MV],[the 'mv' program used for compiling libgticks])
AC_PATH_PROG([MV],[mv],[])
if test "x$MV" = "x"; then
	AC_MSG_NOTICE([==> mv command not found!])
	AC_MSG_NOTICE([==> Set MV variable if present in non-standard path!])
	AC_MSG_ERROR([mv is mandatory: will stop here!])
fi

# Check for 'diff' and get full path.
AC_ARG_VAR([DIFF],[the 'diff' program to use for test output comparison])
AC_PATH_PROG([DIFF],[diff],[])
if test "x$DIFF" = "x"; then
	AC_MSG_NOTICE([==> diff command not found!])
	AC_MSG_NOTICE([==> Set DIFF variable if present in non-standard path!])
	AC_MSG_ERROR([diff is mandatory to run the tests : will stop here!])
fi

AC_TRY_RUN([#include <stdlib.h>
	    int main() { asm ("cpuid"); exit(EXIT_SUCCESS); }],
		 ac_has_cpuid=yes,
		 ac_has_cpuid=no)
if test "$ac_has_cpuid" = yes; then
	AC_MSG_NOTICE(cpuid instruction found and usable)
else
	AC_MSG_NOTICE(cpuid instruction not found or not usable)
fi
AM_CONDITIONAL([HAS_CPUID], [test "$ac_has_cpuid" = yes])

AC_TRY_RUN([#include <stdlib.h>
	    int main() { asm("rdtscp"); exit(EXIT_SUCCESS); }], 
		     ac_has_rdtscp=yes, 
		     ac_has_cpuid=no)
if test "$ac_has_rdtscp" = yes; then
	AC_MSG_NOTICE(rdtscp instruction found and usable)
	AC_DEFINE(HAVE_RDTSCP,1,[Define to 1 if you have the rdtscp assembly instruction])
else
	AC_MSG_NOTICE(rdtscp instruction not found or not usable)
fi

AC_CONFIG_HEADERS([config.h])
AC_CONFIG_FILES([Makefile tests/Makefile src/Makefile libs/Makefile libs/cpuid/Makefile])
AC_PROG_INSTALL

######################################################################
# DOXYGEN SUPPORT
######################################################################

DX_HTML_FEATURE(ON)
DX_CHM_FEATURE(OFF)
DX_CHI_FEATURE(OFF)
DX_MAN_FEATURE(OFF)
DX_RTF_FEATURE(OFF)
DX_XML_FEATURE(OFF)
DX_PDF_FEATURE(OFF)
DX_PS_FEATURE(OFF)

DX_INIT_DOXYGEN([$PACKAGE_NAME],[doxygen.cfg])

######################################################################


AC_OUTPUT

