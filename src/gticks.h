/*
 *  Copyright (c) 2010 Natale Patriciello <65252@studenti.unimore.it>
 *
 *  This file is part of libgticks.
 *
 *   libgticks is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   libgticks is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libgticks.If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef GTICKS
#define GTICKS

/** \file gticks.h
 * Support for get and print CPU ticks.
 *
 * Use gticks to get an uint64_t which contains
 * ticks since booting.\n
 * Use printicks if you want elapsed ticks since a
 * starting point to an ending point, like in the
 * snipped below:
 * \verbatim
uint64_t start, end;
start = gticks();
...
end = gticks();
printicks(start,end,NULL);
\endverbatim
 */
#ifdef HAVE_INTTYPES_H
#include <inttypes.h>
#endif /* HAVE_INTTYPES_H */

#ifdef HAVE_STDINT_H 
#include <stdint.h>
#endif /* HAVE_STDINT_H */

#ifdef HAVE_STDARG_H
#include <stdarg.h>
#endif /* HAVE_STDARG_H */

#ifdef HAVE_STDIO_H
#include <stdio.h>
#endif /* HAVE_STDIO_H */

#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#include <time.h>
#endif /* HAVE_SYS_TIME_H */

#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif /* HAVE_FCNTL_H */

/** \brief Enumeration for supported arch
 *
 * The following architectures are currently supported:
 *
 * -# Linux / x86_32 / gcc
 * -# Linux / x86_64 / gcc
 * -# Linux / ppc-32 / gcc
 * -# OS X / ppc-32 / gcc
 *
 *  Also, libgticks could run without any user intervention
 *  into Mac OS X for Intel computers.\n
 */
typedef enum arch_t {
        /** \brief x86 processors, 32 bit */
        X86_32,
	/** \brief x86_64 processors, 64 bit */
        X86_64,
        /** \brief PowerPC, 32 bit */
        PPC_32,
        /** \brief unsupported arch */
        NOTSUP
} arch_t;

/** \var ARCH
 * The current architecture, selected compile-time.
 */
#if defined(__powerpc__) || defined(__POWERPC__)
const arch_t ARCH = PPC_32;
#elif defined(__x86_64__)
const arch_t ARCH = X86_64;
#elif defined(__i386__)
const arch_t ARCH = X86_32;
#else
const arch_t ARCH = NOTSUP;
#endif /* ARCH selection */

#if defined(HAVE_RDTSCP)
/** \brief Use "rdtscp" as rdtsc if it is present */
# define RDTSC "rdtscp"
/** \brief If rdtscp is present, a cpuid instruction is useless */
# define CPUID ""
#else
# define RDTSC "rdtsc"
# define CPUID "cpuid"
#endif /* HAVE_RDTSCP */

/** \brief Get ticks since booting
 *
 * CPU ticks are, generally, the clock cycles since booting.\n
 * The function isn't serialized, so CPU could re-order the
 * operation's fetching into pipeline.\n
 *
 * \return 0 on unsupported arch, otherwise the ticks since booting.
 * \see gticks_serial
 */
static uint64_t gticks() __attribute__((always_inline));

/** \brief Get ticks from booting and serialize execution
 *
 * CPU ticks are, generally, the clock cycles since booting.\n
 * Force fetching-in-order of operations into pipeline, you must
 * care of overhead of serialize istruction (a cpuid assembly call 
 * on x86_* systems).
 *
 * \return 0 on unsupported arch, otherwise the ticks since booting.
 * \see gticks
 */
static uint64_t gticks_serial() __attribute__((always_inline));

static inline uint64_t gticks()
{
	switch (ARCH) {
		case PPC_32: {
			/*
			* mftbu means "move from time-buffer-upper to result".
			* The loop is saying: tbu=upper, tbl=lower, tbu1=upper,
			* if tbu!=tbu1 there was an overflow so repeat.
			*/
			uint32_t tbl, tbu0, tbu1;
			do {
				asm volatile ("mftbu %0" : "=r"(tbu0));
				asm volatile ("mftb %0" : "=r"(tbl));
				asm volatile ("mftbu %0" : "=r"(tbu1));
			} while (tbu0 != tbu1);
			return (((uint64_t)tbu0) << 32) | tbl;
			break;
		}
		case X86_64: {
			uint32_t hi, lo;
			asm volatile (RDTSC : "=a"(lo), "=d"(hi));
			return ((uint64_t)lo) | (((uint64_t)hi)<<32);
			break;
		}
		case X86_32: {
			uint64_t x;
			__asm__ volatile (RDTSC : "=A" (x));
			return x;
		}
		default:
			return 0;
	}
}

static inline uint64_t gticks_serial()
{
	switch(ARCH) {
		case PPC_32:
			asm volatile("isync" ::: "memory");
			break;
		case X86_32:
		case X86_64:
			asm volatile(CPUID ::: "%eax", "%ebx", "%ecx", "%edx");
			break;
		default:
			asm volatile("" ::: "memory");
	}
	return gticks();
}

/** \brief Print difference between two point
 *
 * Call printf and print end-start.
 *
 * \param start Starting point
 * \param end Ending point
 * \param format (optional) message displayed before the difference print. Use
 * like a printf() format, and append values as you want.
 */
static inline void printicks(uint64_t start, uint64_t end, const char* format, ...)
{
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
	printf("%"PRIu64"\n", end-start);
}

/** \brief Dynamic calculation of register update frequency
 *
 * The dynamic way use clock_gettime as faster resource of
 * reliable time. The function calculate how many instructions
 * are performed by CPU in an interval of one second, and
 * measure this second in nanoseconds with clock_gettime.
 *
 * Then, return KHz, as
 * \verbatim
cpu_khz = cyc * 10^6
          ---------
             ns
\endverbatim
 *
 * \return CPU KHz
 */
static inline uint32_t __dynamic_get_clockfreq ()
{
	if (ARCH == NOTSUP)
		return 0;
	struct timespec ts, te;
	uint32_t reg_khz;
	uint64_t cycles[2], ns;

	clock_gettime(CLOCK_MONOTONIC, &ts);
	cycles[0] = gticks_serial();

	clock_gettime(CLOCK_MONOTONIC, &ts);
	cycles[0] = gticks_serial();

	usleep(1000);

	cycles[1] = gticks_serial();
	clock_gettime(CLOCK_MONOTONIC, &te);

	ns = ((te.tv_sec-ts.tv_sec)*1000000000) +
		(te.tv_nsec-ts.tv_nsec);

	reg_khz = ((cycles[1] - cycles[0])*1000000) / (ns);
	return reg_khz;
}

/** \brief Static calculation of register update frequency
 *
 * \par X86, 32 and 64 bit
 *
 * Read /proc/cpuinfo and get the system MHz, and return cpu_mhz*1000.
 *
 * \par PowerPC 32 bit
 *
 * Read /proc/cpuinfo and get the timebase frequency (in Hz), and return
 * timebase / 1000.
 *
 */
static inline uint32_t __static_get_clockfreq ()
{
	switch (ARCH) {
		case NOTSUP:
			return 0;
			break;
		case PPC_32: {
			/* We read the information from the /proc filesystem.
			 * /proc/cpuinfo  contains at least one line like:
			 * timebase   : 33333333
			 * We search for this line and convert the number
			 * into an integer.
			 */
			static uint32_t timebase_freq;
			uint32_t result = 0L;

			/* If this function was called before, we know the result.*/
			if (timebase_freq != 0)
				return timebase_freq;

			int fd = open ("/proc/cpuinfo", O_RDONLY);
				if (fd != -1) {
				/* The timebase will be in the 1st 1024 bytes for systems with up
				 * to 8 processors.  If the first read returns less then 1024
				 * bytes read,  we have the whole cpuinfo and can start the scan.
				 * Otherwise we will have to read more to insure we have the
				 * timebase value in the scan.
				 */
				char buf[1024];
				ssize_t n;
				n = read (fd, buf, sizeof (buf));
				if (n == sizeof (buf)) {
					/* We are here because the 1st read returned exactly sizeof
					(buf) bytes.  This implies that we are not at EOF and may
					not have read the timebase value yet.  So we need to read
					more bytes until we know we have EOF.  We copy the lower
					half of buf to the upper half and read sizeof (buf)/2
					bytes into the lower half of buf and repeat until we
					reach EOF.  We can assume that the timebase will be in
					the last 512 bytes of cpuinfo, so two 512 byte half_bufs
					will be sufficient to contain the timebase and will
					handle the case where the timebase spans the half_buf
					boundry.  */
					const ssize_t half_buf = sizeof (buf) / 2;
					while (n >= half_buf) {
						memcpy (buf, buf + half_buf, half_buf);
						n = read (fd, buf + half_buf, half_buf);
					}
					if (n >= 0)
						n += half_buf;
				}

				if (n > 0) {
					char *mhz = (char*) strstr (buf, "timebase");

					if (__builtin_expect (mhz != NULL, 1)){
						char *endp = buf + n;

						/* Search for the beginning of the string.  */
						while (mhz < endp && (*mhz < '0' || *mhz > '9')
								&& *mhz != '\n')
							++mhz;

						while (mhz < endp && *mhz != '\n') {
							if (*mhz >= '0' && *mhz <= '9') {
								result *= 10;
								result += *mhz - '0';
							}
							++mhz;
						}
					}
					timebase_freq = result;
				}
				close (fd);
			}
			return timebase_freq/1000;
			break;
			}
		case X86_32:
		case X86_64: {
			/* We read the information from the /proc filesystem.  It contains at
			* least one line like
			* cpu MHz    : 497.840237
			* or also
			* cpu MHz    : 497.841
			* We search for this line and convert the number in an integer.*/
			static uint32_t result;
			int fd;

			/* If this function was called before, we know the result.  */
			if (result != 0)
				return result;

			fd = open ("/proc/cpuinfo", O_RDONLY);
			if (__builtin_expect (fd != -1, 1)) {
				/* XXX AFAIK the /proc filesystem can generate "files" only up
					to a size of 4096 bytes.  */
				char buf[4096];
				ssize_t n;

				n = read (fd, buf, sizeof buf);
				if (__builtin_expect (n, 1) > 0) {
					char *mhz = (char*) strstr (buf, "cpu MHz");

					if (__builtin_expect (mhz != NULL, 1)) {
						char *endp = buf + n;
						int seen_decpoint = 0;
						int ndigits = 0;

						/* Search for the beginning of the string.  */
						while (mhz < endp && (*mhz < '0' || *mhz > '9') && *mhz != '\n')
							++mhz;

						while (mhz < endp && *mhz != '\n') {
							if (*mhz >= '0' && *mhz <= '9') {
								result *= 10;
								result += *mhz - '0';
								if (seen_decpoint)
								  ++ndigits;
							} else if (*mhz == '.')
								seen_decpoint = 1;
							++mhz;
						}
						/* Compensate for missing digits at the end.  */
						while (ndigits++ < 6)
							result *= 10;
					}
				}

				close (fd);
			}
			return result/1000;
			break;
			}
	}
	return 0;
}

/** \brief Static scale used for calculating nanoseconds from CPU cycles
 *
 * \see __scale_upd
 */
#define CYC2NS_SCALE_FACTOR 10 /* 2^10, carefully chosen */

/** \brief Update the scale used to calculate ns from cyc
 *
 * Obtain the update frequency for the register, and calculate
 * cyc2ns_scale.
 *
 * \see cyc2ns
 */
static inline uint64_t __scale_upd()
{
	static uint64_t cpu_scale = 0;
	uint32_t freq;

	if (ARCH == NOTSUP)
		return 0;
	else if (cpu_scale != 0)
		return cpu_scale;

#ifdef DYNAMIC_REGFREQ
	freq = __dynamic_get_clockfreq();
#else
	freq = __static_get_clockfreq();
#endif /* DYNAMIC_REGFREQ */
	cpu_scale = (1000000 << CYC2NS_SCALE_FACTOR) / freq;

	return cpu_scale;
}

/** \brief Calculate nanoseconds from CPU cycles
 *
 * Convert from cycles(64bits) => nanoseconds (64bits)\n
 * Basic equation (Note: freq is the frequency at which the register
 * is updated, and not cpu speed):\n
 *              ns = cycles / (freq / ns_per_sec)\n
 *              ns = cycles * (ns_per_sec / freq)\n
 *              ns = cycles * (10^9 / (reg_khz * 10^3))\n
 *              ns = cycles * (10^6 / reg_khz)\n
 *
 * Then we use scaling math to get:\n
 *              ns = cycles * (10^6 * SC / reg_khz) / SC \n
 *              ns = cycles * cyc2ns_scale / SC \n
 *
 * And since SC is a constant power of two, we can convert the div
 * into a shift.
 *
 * We can use khz divisor instead of mhz to keep a better precision, since
 * cyc2ns_scale is limited to 10^6 * 2^10, which fits in 32 bits.
 *
 * cyc2ns_scale is calculated by __scale_upd.
 *
 * \see __scale_upd
 * \see __dynamic_get_clockfreq
 * \see __static_get_clockfreq
 * \param cyc Cycles
 * \return Nanoseconds 
 */
static inline uint64_t cyc2ns(uint64_t cyc)
{
	return cyc * __scale_upd() >> CYC2NS_SCALE_FACTOR;
}

#endif /* GTICKS */
