/*
 *  Copyright (c) 2010 Natale Patriciello <65252@studenti.unimore.it>
 *
 *  This file is part of libgticks.
 *
 *   libgticks is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   libgticks is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libgticks.If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */

#ifdef HAVE_STDIO_H
#include <stdio.h>
#endif /* HAVE_STDIO_H */

#ifdef HAVE_INTTYPES_H
#include <inttypes.h>
#endif /* HAVE_INTTYPES_H */

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))
#define IS_AVAIL(extension, max) (max>=extension)
static int tsc_presence()
{
	uint32_t out;
	asm (	"mov $0x1, %%eax\n\t"
		"cpuid"
		: "=d" (out)
		:
		: "%eax");

	return CHECK_BIT(out,5);
}

static uint32_t cpuid_extended()
{
	uint32_t out;
	asm ("mov $0x80000000, %%eax\n\t"
		"cpuid"
		: "=a" (out));

	return out;
}

static int rdtscp_presence()
{
	uint32_t out;
	asm (	"mov $0x80000001, %%eax\n\t"
		"cpuid"
		: "=d" (out)
		:
		: "%eax");

	return !CHECK_BIT(out,28);
}

static int tsc_invariant()
{
	uint32_t out;
	asm(	"mov $0x80000007, %%eax\n\t"
		"cpuid"
		: "=d" (out)
		:
		: "%eax");

	return CHECK_BIT(out,9);
}

int main(int argc, char **argv)
{
	printf("Checking tsc presence...");
	if (tsc_presence())
		printf(" present!\n");
	else
		printf(" not present :(\n");

	printf ("Checking maximun param for cpuid...");
	uint64_t max_ex = cpuid_extended();
	printf ("is %lX\n", max_ex);

	/* TODO: This checking is true only for AMD. Intel lies.. */

	printf ("Checking for rdtscp...");
	if (IS_AVAIL(0x80000001, max_ex)) {
		if (rdtscp_presence())
			printf ("present!\n");
		else
			printf (" not present :(\n");
	}

	printf ("Checking for invariant tsc...");
	if (IS_AVAIL(0x8000007, max_ex)) {
		if (tsc_invariant())
			printf("present!\n");
		else
			printf(" not present :(\n");
	}

	exit(EXIT_SUCCESS);
}
